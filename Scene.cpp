/* 
 * File:   Scene.cpp
 * Author: ciapunek
 * 
 * Scene (renderer)
 * 
 * Created on June 4, 2012, 1:37 PM
 */

#include "Scene.h"
#include "Util.h"
#include "Gl.h"

#include "map/Moonhills.h"
#include "light/Punctual.h"
#include "light/Directional.h"

namespace bx {

    Scene::Scene() {
    }

    Scene::~Scene() {
        if (camera) delete camera;
        if (projector) delete projector;

        for (int i = 0; i < objects.size(); i++) {
            if (objects[i]) delete objects[i];
        }

        for (int i = 0; i < lights.size(); i++) {
            if (lights[i]) delete lights[i];
        }
    }

    Camera * Scene::getCamera() {
        return camera;
    }

    Projector * Scene::getProjector() {
        return projector;
    }

    bool Scene::init() {

        // OpenGL
        GLenum glew_status = glewInit();
        if (glew_status != GLEW_OK) {
            Util::debug("GLEW initialization failed");
            return false;
        }

        if (!GLEW_VERSION_2_0) {
            Util::debug("GLEW 2.0 is required");
            return false;
        }

        // World service
        Util::debug("Initializing camera");
        camera = new Camera();

        Util::debug("Initializing projector");
        projector = new Projector();

        // Objects
        Util::debug("Loading map");

        map = new bx::map::Moonhills();
        map->use(this);

        Util::debug("Setup objects and lights for drawning");
        for (int i = 0; i < lights.size(); i++) {
            if (lights[i]->getType() == Light::Punctual) {
                object::Pointlight * light = new object::Pointlight();
                light->load();
                light->upload();

                light->getMaterial()->setDiffuseColor(lights[i]->getDiffuseColor());
                light->setPosition(lights[i]->getPosition().x, lights[i]->getPosition().y, lights[i]->getPosition().z);
                addObject(light);
            }
        }

        for (int i = 0; i < objects.size(); i++) {
            objects[i]->init();
            Shader * shader = objects[i]->getShader();
            if (shader->isError()) {
                Util::debug("Object '" + objects[i]->getName() + "' shader error");
                Util::debug(shader->getLinkLog());
                Util::debug(shader->getCompileLog());
            }

            attachLights(shader);
        }

        return true;
    }

    /**
     * 
     * @param width
     * @param height
     */
    void Scene::resize(int width, int height) {
        this->width = width;
        this->height = height;
        this->aspect = (GLfloat) width / (GLfloat) height;

        glViewport(0, 0, width, height);
    }

    /**
     * Step over all actors (moveable objects)
     * 
     * @param time
     */
    void Scene::step(float time) {
        for (int i = 0; i < actors.size(); i++) {
            actors[i]->step(time);
        }
    }

    /**
     * Draw all objects (visible and renderable)
     */
    void Scene::draw() {
        drawInit();
        drawObjects();
    }

    /**
     * Add moveable object
     * 
     * @param actor
     */
    void Scene::addActor(Actor * actor) {
        actors.push_back(actor);
        addObject(actor);
    }

    /**
     * Add static object
     * 
     * @param scenery
     */
    void Scene::addScenery(Scenery * scenery) {
        sceneries.push_back(scenery);
        addObject(scenery);
    }

    /**
     * Add object to internal list
     * 
     * @param object
     */
    void Scene::addObject(Object * object) {
        objects.push_back(object);
    }

    /**
     * Draw initializing
     */
    void Scene::drawInit() {

        // Clear buffers
        glClearColor(0.07f, 0.12f, 0.22f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    /**
     * Draw all objects
     */
    void Scene::drawObjects() {

        // Enable depth test
        glEnable(GL_DEPTH_TEST);

        // Accept fragment if it closer to the camera than the former one
        glDepthFunc(GL_LESS);

        // Cull triangles which normal is not towards the camera
        glEnable(GL_CULL_FACE);

        glCullFace(GL_BACK);

        for (int i = 0; i < objects.size(); i++) {
            if (objects[i]->isVisible()) {
                objects[i]->draw(projector, camera);
            }
        }
    }

    /**
     * Remove object from scene
     */
    bool Scene::removeObject(Object * object) {
        for (int i = 0; i < objects.size(); i++) {
            if (objects[i] == object) {
                objects.erase(objects.begin() + i);
                return true;
            }
        }
        return false;
    }

    /**
     * Add light to scene
     */
    void Scene::addLight(Light * light) {
        lights.push_back(light);
    }

    /**
     * Remove light from scene
     */
    bool Scene::removeLight(Light * light) {
        for (int i = 0; i < lights.size(); i++) {
            if (lights[i] == light) {
                lights.erase(lights.begin() + i);
                return true;
            }
        }
        return false;
    }

    /**
     * Setup lights for one of object shader
     */
    void Scene::attachLights(Shader * shader) {

        struct lightParams {
            glm::vec3 attenuation;
            float padding[1];
            glm::vec4 diffuseColor;
            glm::vec4 specularColor;
            glm::vec4 position;
        };

        const int MAX_LIGHTS = 8;

        struct shaderLight {
            glm::vec4 ambientColor;
            lightParams lightParameters[MAX_LIGHTS];
        };

        shaderLight lightSources;
        
        if (lights[0]->getType() == Light::Directional) {
            
            light::Directional * light = (light::Directional *) lights[0];
            lightSources.ambientColor = light->getAmbientColor();
            lightSources.lightParameters[0].attenuation = glm::vec3(0, 0, 0);
            lightSources.lightParameters[0].diffuseColor = light->getDiffuseColor();
            lightSources.lightParameters[0].specularColor = light->getSpecularColor();
            lightSources.lightParameters[0].position = glm::vec4(light->getPosition(), 0.0);
        }
        
        for (int i = 1; i < lights.size(); i++) {
            if (lights[i]->getType() == Light::Punctual) {
                light::Punctual * light = (light::Punctual *) lights[i];
                
                lightSources.lightParameters[i].attenuation = light->getAttenuation();
                lightSources.lightParameters[i].diffuseColor = light->getDiffuseColor();
                lightSources.lightParameters[i].specularColor = light->getSpecularColor();
                lightSources.lightParameters[i].position = glm::vec4(light->getPosition(), 1.0);
            }
        }

        // Init
        glGenBuffers(1, &lightsBuffer);
        glBindBuffer(GL_UNIFORM_BUFFER, lightsBuffer);
        glBufferData(GL_UNIFORM_BUFFER, sizeof (shaderLight), NULL, GL_STATIC_DRAW);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        // Upload
        glBindBuffer(GL_UNIFORM_BUFFER, lightsBuffer);
        glBufferData(GL_UNIFORM_BUFFER, sizeof (shaderLight), &lightSources, GL_STATIC_DRAW);
        glUniformBlockBinding(shader->getProgramId(), shader->getUniformBlockLocation("lightSource"), 0);
        glBindBufferBase(GL_UNIFORM_BUFFER, 0, lightsBuffer);
    }

    /**
     * Set player to be movable by input methods
     * 
     * @param player
     */
    void Scene::setPlayer(Player * player) {
        this->player = player;
    }

    /**
     * Get currently controlled player
     */
    Player * Scene::getPlayer() {
        return player;
    }
}
