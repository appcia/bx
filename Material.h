/* 
 * File:   Material.h
 * Author: hayalet
 *
 * Created on 28 czerwiec 2012, 15:32
 */

#ifndef MATERIAL_H
#define	MATERIAL_H

#include "Resource.h"
#include "Texture.h"

#include "Gl.h"

#include <string>

using namespace std;

namespace bx {

    class Material : public Resource {
    public:
        Material();
        virtual ~Material();

        Material * clone() {
            return new Material(*this);
        }

        static Material * loadMtl(string path);

        void loadTexture(string path);
        void loadNormalTexture(string path);

        float getShininess();
        float getIllumination();
        glm::vec4 getAmbientColor();
        glm::vec4 getDiffuseColor();
        glm::vec4 getSpecularColor();

        void setDiffuseColor(glm::vec4 diffuseColor);

        Texture * getTexture();
        Texture * getNormalTexture();
        bool isNormalTexture();

        bool isBumpMapping();

    protected:
        string path; /* .mtl file */

        float shininess; /* Ns */
        glm::vec4 ambientColor; /* Ka */
        glm::vec4 diffuseColor; /* Kd */
        glm::vec4 specularColor; /* Ks */
        bool illumination; /* illum */ /* 0 - no specular highlights, 1 - yes */

        Texture * texture;
        Texture * normalTexture;
        bool bumpMapping;
    };

}

#endif	/* MATERIAL_H */

