/* 
 * File:   Scenery.h
 * Author: ciapunek
 *
 * Created on August 7, 2012, 10:59 PM
 */

#ifndef SCENERY_H
#define	SCENERY_H

#include "Object.h"

namespace bx {

    class Scenery : public Object {
    public:
        Scenery();
        Scenery(const Scenery& orig);
        virtual ~Scenery();
    private:

    };
}


#endif	/* SCENERY_H */

