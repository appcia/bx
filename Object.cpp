/* 
 * File:   Object.cpp
 * Author: ciapunek
 * 
 * Created on July 3, 2012, 10:48 PM
 */

#include "Object.h"

namespace bx {

    Object::Object() {
        name = "Object";

        scaleX = scaleY = scaleZ = 1.0;
        rotationX = rotationY = rotationZ = 0.0;
        identityMatrix = glm::mat4(1.0);

        visible = true;
        renderable = true;
    }

    Object::~Object() {
        if (material) delete material;
        if (model) delete model;
        if (shader) delete shader;
        if (boundingBox) delete boundingBox;
    }

    string Object::getName() {
        return name;
    }

    void Object::setShader(Shader * shader) {
        this->shader = shader;
    }

    Shader * Object::getShader() {
        return shader;
    }

    Model * Object::getModel() {
        return model;
    }

    Material * Object::getMaterial() {
        return material;
    }

    /**
     * Upload all required data to graphic card
     */
    void Object::upload() {
        model->upload(shader);

        if (material->getTexture() != NULL) material->getTexture()->upload();
        if (material->getNormalTexture() != NULL) material->getNormalTexture()->upload();
    }

    /**
     * Draw object
     * 
     * @param Projector
     * @param Camera
     */
    void Object::draw(Projector * projector, Camera * camera) {
        shader->start();
        material->getTexture()->start();

        model->send(this, projector, camera);
        model->draw();

        material->getTexture()->stop();
        shader->stop();
    }

    void Object::init() {
        this->boundingBox = new BoundingBox(this->model->getVertices(), identityMatrix);
    }

    void Object::setScale(float scaleX, float scaleY, float scaleZ) {
        this->scaleX = scaleX;
        this->scaleY = scaleY;
        this->scaleZ = scaleZ;
    }

    glm::vec3 Object::getScale() {
        return glm::vec3(scaleX, scaleY, scaleZ);
    }

    void Object::setScale(float scale) {
        scaleX = scaleY = scaleZ = scale;
    }

    glm::mat4 Object::getMatrix() {
        glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), this->getPosition());
        glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0f), this->getScale());
        glm::mat4 rotationXMatrix = glm::rotate(glm::mat4(1.0f), rotationX, glm::vec3(1.0, 0, 0));
        glm::mat4 rotationYMatrix = glm::rotate(glm::mat4(1.0f), rotationY, glm::vec3(0, 1.0, 0));
        glm::mat4 rotationZMatrix = glm::rotate(glm::mat4(1.0f), rotationZ, glm::vec3(0, 0, 1.0));
        identityMatrix = translationMatrix * rotationYMatrix * rotationZMatrix * rotationXMatrix * scaleMatrix;

        return identityMatrix;
    }

    bool Object::check2Dcollisions(Object * object1, Object * object2) {
        BoundingBox::Rectangle rect1 = object1->boundingBox->get2DRectangle();
        BoundingBox::Rectangle rect2 = object2->boundingBox->get2DRectangle();
        if(rect1.x >= rect2.x + rect2.width || rect1.x + rect1.width <= rect2.x || rect1.y >= rect2.y + rect2.height || rect1.y + rect1.height <= rect2.y) {
            return false;
        }
        return true;
    }
    
    bool Object::isVisible() {
        return this->visible;
    }

    void Object::show() {
        this->visible = true;
    }

    void Object::hide() {
        this->visible = false;
    }
    
    void Object::setRotationX(float rotation) {
        this->rotationX = rotation;
    }

    float Object::getRotationX() const {
        return rotationX;
    }

    void Object::setRotationY(float rotation) {
        this->rotationY = rotation;
    }

    float Object::getRotationY() const {
        return rotationY;
    }

    void Object::setRotationZ(float rotation) {
        this->rotationZ = rotation;
    }

    float Object::getRotationZ() const {
        return rotationZ;
    }

    void Object::setScaleX(float scale) {
        this->scaleX = scale;
    }

    float Object::getScaleX() const {
        return scaleX;
    }

    void Object::setScaleY(float scale) {
        this->scaleY = scale;
    }

    float Object::getScaleY() const {
        return scaleY;
    }

    void Object::setScaleZ(float scale) {
        this->scaleZ = scale;
    }

    float Object::getScaleZ() const {
        return scaleZ;
    }

}


