/* 
 * File:   BoundingBox.h
 * Author: hayalet
 *
 * Created on August 9, 2012, 21:02 PM
 */

#ifndef BOUNDINGBOX_H
#define	BOUNDINGBOX_H

#include <string>
#include <cstdio>
#include <vector>
#include "Gl.h"

using namespace std;

namespace bx {

    class BoundingBox {
    public:
        struct Rectangle {
            float x;
            float y;
            float width;
            float height;
            Rectangle(float x, float y, float width, float height)
            {
                this->x = x;
                this->y = y;
                this->width = width;
                this->height = height;
            }
        };
        
        BoundingBox(const vector <glm::vec3> vertices, const glm::mat4 matrix);
        virtual ~BoundingBox();
        
        void calculateCenter();
        void calculateSize();
        void transform(const glm::mat4 matrix);
        
        Rectangle get2DRectangle();
        bool check2DCollision(BoundingBox * that); 
    private:
        glm::vec3 initmin;
        glm::vec3 initmax;
        glm::vec3 min;
        glm::vec3 max;
        glm::vec3 size;
        glm::vec3 center;
    };
}


#endif	/* BOUNDINGBOX_H */

