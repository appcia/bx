/* 
 * File:   Point.cpp
 * Author: ciapunek
 * 
 * Created on August 12, 2012, 12:55 AM
 */

#include "Point.h"

namespace bx {

    Point::Point() {
        x = y = z = 0.0;
    }

    Point::~Point() {
    }

    glm::vec3 Point::getPosition() {
        return glm::vec3(x, y, z);
    }

    void Point::setPosition(float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    void Point::setX(float x) {
        this->x = x;
    }

    float Point::getX() const {
        return x;
    }

    void Point::setY(float y) {
        this->y = y;
    }

    float Point::getY() const {
        return y;
    }

    void Point::setZ(float z) {
        this->z = z;
    }

    float Point::getZ() const {
        return z;
    }

}
