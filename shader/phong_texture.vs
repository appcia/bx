#version 330 core

const int MAX_LIGHTS = 8;
const int LIGHT_NUM = 5;

in vec3 Vertex;
in vec2 uvCoords;
in vec3 Normals;

out vec4 iV;
out vec4 iN;
out vec4 iL[MAX_LIGHTS];
out vec2 UV;

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

struct lightParams {
    vec3 attenuation;
    vec4 diffuseColor;
    vec4 specularColor;
    vec4 position;
};

layout(std140) uniform light {
    vec4 ambientColor;
    lightParams lightParameters[MAX_LIGHTS];
    float padding[1];
} lightSource;

uniform float shininess;

void main() {
   gl_Position = P * V * M * vec4(Vertex,1);
   iN = normalize(V * M * vec4(Normals,0));
   iV = normalize(vec4(0,0,0,1) - V * M * vec4(Vertex,1));
   int i;
   for(i = 0; i < LIGHT_NUM; i++){
       iL[i] = (V * lightSource.lightParameters[i].position - V * M * vec4(Vertex,1));
   }
   UV = uvCoords;
}