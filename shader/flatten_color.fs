#version 330 core

out vec4 color;

uniform struct materialParameters {
     vec4 ambientColor;
     vec4 diffuseColor;
     vec4 specularColor;
     float illumination;
     float shininess;
} material;
 
void main(){     
       color += material.diffuseColor;
}
