#version 330 core

in vec3 Vertex;
in vec2 uvCoords;

out vec2 UV;

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

void main() {
   gl_Position = P * V * M * vec4(Vertex,1);
   UV = uvCoords;
}
