#version 330 core

const int MAX_LIGHTS = 8;
const int LIGHT_NUM = 5;

in vec4 iV;
in vec4 iN;
in vec4 iL[MAX_LIGHTS];

out vec4 color;

struct lightParams {
    vec3 attenuation;
    vec4 diffuseColor;
    vec4 specularColor;
    vec4 position;
};

layout(std140) uniform light {
    vec4 ambientColor;
    lightParams lightParameters[MAX_LIGHTS];
    float padding[1];
} lightSource;

uniform struct materialParameters {
     vec4 ambientColor;
     vec4 diffuseColor;
     vec4 specularColor;
     float illumination;
     float shininess;
} material;
 
void main(){
   vec4 eyeN=normalize(iN);
   vec4 eyeV=normalize(iV);

   int i;
   for(i = 0; i < LIGHT_NUM; i++) {
       vec4 eyeL = normalize(normalize(iL[i]));
       vec4 eyeR = reflect(-eyeL,eyeN);

       float attenuation;
       if (lightSource.lightParameters[i].position.w == 0.0) {
           eyeL = normalize(lightSource.lightParameters[i].position);
           attenuation = 1.0;
       } else {
           float distance = length(iL[i]);
           attenuation = 1.0 / (lightSource.lightParameters[i].attenuation.x
                                + lightSource.lightParameters[i].attenuation.y * distance
                                + lightSource.lightParameters[i].attenuation.z * distance * distance);
                               
        }
       
       color += attenuation * (lightSource.lightParameters[i].diffuseColor * material.diffuseColor * max(0,dot(eyeL,eyeN)));

       if(material.illumination == 1.0){
            color += attenuation * (material.specularColor * lightSource.lightParameters[i].specularColor * pow(max(0,dot(eyeR,eyeN)),material.shininess));
       }
   }
   color += lightSource.ambientColor * material.ambientColor;
}
