/* 
 * File:   Shader.cpp
 * Author: ciapunek
 * 
 * Shaders manager
 * 
 * Created on June 9, 2012, 11:37 PM
 */

#include "Shader.h"
#include "Common.h"

#include <fstream>
#include <istream>

namespace bx {

    Shader::Shader() {
        type = "Shader";
        error = false;
        programId = 0;
        compileLog = "";
        linkLog = "";
    }

    Shader::~Shader() {
        if (programId) {
            glDeleteProgram(programId);
            programId = 0;
        }
    }

    Shader * Shader::load(string vsPath, string fsPath, string gsPath) {
        int hash = Resource::generateHash("shader_" + vsPath + fsPath + gsPath);
        Storage * storage = App::instance()->getStorage();
        Shader * shader = (Shader *) storage->getResource(hash);

        // Storage hit
        if (shader != NULL) {
            return shader;
        }

        // Load, compile shader and add to storage
        shader = new Shader();

        if (!vsPath.empty()) {
            shader->compile(vsPath, Shader::Vertex);
        }

        if (!fsPath.empty()) {
            shader->compile(fsPath, Shader::Fragment);
        }

        if (!gsPath.empty()) {
            shader->compile(gsPath, Shader::Geometry);
        }

        shader->link();

        storage->addAsReference(shader, hash);
        return shader;
    }

    void Shader::send(Projector * projector, Camera * camera, Object * object) {
        glUniformMatrix4fv(getUniformLocation("P"), 1, GL_FALSE, glm::value_ptr(projector->getMatrix()));
        glUniformMatrix4fv(getUniformLocation("V"), 1, GL_FALSE, glm::value_ptr(camera->getMatrix()));
        glUniformMatrix4fv(getUniformLocation("M"), 1, GL_FALSE, glm::value_ptr(object->getMatrix()));

        Material * material = object->getMaterial();
        
        glm::vec4 color = material->getAmbientColor();
        glUniform4f(getUniformLocation("material.ambientColor"), color.x, color.y, color.z, color.w);
        color = material->getDiffuseColor();
        glUniform4f(getUniformLocation("material.diffuseColor"), color.x, color.y, color.z, color.w);
        color = material->getSpecularColor();
        glUniform4f(getUniformLocation("material.specularColor"), color.x, color.y, color.z, color.w);
        glUniform1f(getUniformLocation("material.illumination"), material->getIllumination());
        glUniform1f(getUniformLocation("material.shininess"), material->getShininess());
    }

    string Shader::readSource(string path) {
        string code;
        ifstream stream(path.c_str(), ios::in);
        if (stream.is_open()) {
            string line = "";
            while (getline(stream, line)) {
                code += "\n" + line;
            }
            stream.close();
        }
        return code;
    }

    bool Shader::compile(string path, Type type) {

        GLuint id = glCreateShader(type);
        string source = readSource(path);

        GLint result = GL_FALSE;
        int logLength;

        char const * sourcePointer = source.c_str();
        glShaderSource(id, 1, &sourcePointer, NULL);
        glCompileShader(id);

        glGetShaderiv(id, GL_COMPILE_STATUS, &result);
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &logLength);

        vector<char> logInfo(logLength);
        glGetShaderInfoLog(id, logLength, NULL, &logInfo[0]);

        compileLog += &logInfo[0];
        compileLog += "\n";

        if (result) {
            ids.push_back(id);
            return true;
        }

        error = true;
        return false;
    }

    bool Shader::link() {

        // Check previously created program
        if (programId) {
            glDeleteProgram(programId);
            programId = 0;
        }

        // Try to create new
        programId = glCreateProgram();
        if (!programId) {
            return false;
        }

        // Attach compiled shader resources and link program
        for (int i = 0; i < ids.size(); i++) {
            glAttachShader(programId, ids[i]);
        }
        glLinkProgram(programId);

        // Validate, get log messages
        GLint result = GL_FALSE;
        int logLength;

        glGetProgramiv(programId, GL_LINK_STATUS, &result);
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &logLength);

        vector<char> logInfo(max(logLength, int(1)));
        glGetProgramInfoLog(programId, logLength, NULL, &logInfo[0]);

        linkLog += &logInfo[0];
        linkLog += "\n";

        // Clear shader resources
        for (int i = 0; i < ids.size(); i++) {
            glDeleteShader(ids[i]);
        }

        if (!result) {
            error = true;
        }

        return result;
    }

    void Shader::start() {
        glUseProgram(programId);
    }

    void Shader::stop() {
        glUseProgram(0);
    }

    string Shader::getCompileLog() {
        return compileLog;
    }

    string Shader::getLinkLog() {
        return linkLog;
    }

    GLuint Shader::getProgramId() {
        return programId;
    }

    bool Shader::isError() {
        return error;
    }

    GLuint Shader::getUniformLocation(string varname) {
        return glGetUniformLocation(programId, varname.c_str());
    }

    GLuint Shader::getAttribLocation(string varname) {
        return glGetAttribLocation(programId, varname.c_str());
    }

    GLuint Shader::getUniformBlockLocation(string varname) {
        return glGetUniformBlockIndex(programId, varname.c_str());
    }
}