/* 
 * File:   Util.h
 * Author: ciapunek
 *
 * Created on July 5, 2012, 7:51 PM
 */

#ifndef UTIL_H
#define	UTIL_H

#include <string>
#include <cstring>
#include <cstdio>

using namespace std;

namespace bx {

    class Util {
    public:
        static void logTime();
        static void print(string format, ...);
        static void debug(string text);
        
        static string intToStr(int value);
        static int strToInt(string value);

        static string floatToStr(float value);
        static int strToFloat(string value);

        static int hashInt(string value);
    private:
    };
}


#endif	/* UTIL_H */

