/* 
 * File:   Config.h
 * Author: ciapunek
 *
 * Created on June 4, 2012, 2:45 PM
 */

#ifndef CONFIG_H
#define	CONFIG_H

#include "Util.h"

#include <string>
#include <map>

using namespace std;

namespace bx {

    class Config {
    public:
        Config();
        Config(const Config& orig);
        virtual ~Config();

        static Config * loadDefault();
        static Config * loadFromFile(string filename);
        
        string operator[](string name);
        
        void set(string name, string value);
        string get(string name);

        void setInt(string name, int value);
        int getInt(string name);
        
        void setFloat(string name, float value);
        float getFloat(string name);

    private:
        map<string, string> data;
    };
}

#endif	/* CONFIG_H */

