/* 
 * File:   Camera.cpp
 * Author: ciapunek
 * 
 * Created on June 8, 2012, 2:10 PM
 */

#include "Camera.h"
#include "Object.h"

namespace bx {

    Camera::Camera() {
        setDefaults();
    }

    Camera::Camera(const Camera& orig) {
    }

    Camera::~Camera() {
    }

    /**
     * Default camera settings
     */
    void Camera::setDefaults() {
        hAngle = 135.0f;
        vAngle = 0.0f;
    }

    /**
     * Changing direction
     * 
     * Is using spherical coordinates to Cartesian coordinates conversion
     * 
     * @param hAngleDelta Horizontal angle delta
     * @param vAngleDelta Vertical angle delta
     */

    void Camera::rotate(float hAngleDelta, float vAngleDelta) {
        if (vAngle > M_PI / 2)
            vAngle = M_PI / 2;
        if (vAngle<-M_PI / 2)
            vAngle = -M_PI / 2;
        if (hAngle < 0.0)
            hAngle += 2 * M_PI;
        if (hAngle > 2 * M_PI)
            hAngle -= 2 * M_PI;

        hAngle += hAngleDelta;
        vAngle += vAngleDelta;

        direction = glm::vec3(position.x + sin(hAngle), position.y + sin(vAngle), position.z + cos(hAngle));
    }

    void Camera::moveCamera() {
        matrix = glm::lookAt(
            position,
            direction,
            glm::vec3(0, 1, 0)
        );
    }

    void Camera::follow(Object * object, float zoom, float height) {
        position = glm::vec3(object->getPosition().x, object->getPosition().y + height, object->getPosition().z + zoom);
        direction = glm::vec3(position.x, position.y, position.z - zoom);
    }

    glm::mat4 Camera::getMatrix() {
        return matrix;
    }

    /**
     * Move camera position up
     * 
     * @param delta
     */
    void Camera::moveUp(float delta) {
        position.x -= delta * -sin(hAngle) * cos(vAngle);
        position.y -= delta * -sin(vAngle);
        position.z -= delta * -cos(hAngle) * cos(vAngle);
        direction.x = position.x + sin(hAngle);
        direction.y = position.y + sin(vAngle);
        direction.z = position.z + cos(hAngle);
    }

    /**
     * Move camera position down
     * 
     * @param delta
     */
    void Camera::moveDown(float delta) {
        position.x += delta * -sin(hAngle) * cos(vAngle);
        position.y -= delta * sin(vAngle);
        position.z += delta * -cos(hAngle) * cos(vAngle);
        direction.x = position.x + sin(hAngle);
        direction.y = position.y + sin(vAngle);
        direction.z = position.z + cos(hAngle);
    }

    /**
     * Move camera position left
     * 
     * @param delta
     */
    void Camera::moveLeft(float delta) {
        position.x -= delta * -cos(hAngle);
        position.z += delta * -sin(hAngle);
        direction.x -= delta * -cos(hAngle);
        direction.y = position.y + sin(vAngle);
        direction.z += delta * -sin(hAngle);
    }

    /**
     * Move camera position right
     * 
     * @param delta
     */
    void Camera::moveRight(float delta) {
        position.x += delta * -cos(hAngle);
        position.z -= delta * -sin(hAngle);
        direction.x += delta * -cos(hAngle);
        direction.y = position.y + sin(vAngle);
        direction.z -= delta * -sin(hAngle);
    }
}
