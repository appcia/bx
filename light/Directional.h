/* 
 * File:   Directional.h
 * Author: ciapunek
 *
 * Created on August 12, 2012, 1:58 AM
 */

#ifndef DIRECTIONAL_H
#define	DIRECTIONAL_H

#include "../Light.h"

namespace bx {
    namespace light {

        class Directional : public Light {
        public:
            Directional();
            Directional(const Directional& orig);
            virtual ~Directional();
            
            void setAmbientColor(float r, float g, float b, float alpha);
            glm::vec4 getAmbientColor();

            void setColor(float r, float g, float b, float alpha);
        private:
            glm::vec4 ambientColor;
        };
    }

}


#endif	/* DIRECTIONAL_H */

