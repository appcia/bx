/* 
 * File:   Point.h
 * Author: hayalet
 *
 * Created on 11 sierpień 2012, 17:10
 */

#ifndef POINT_H
#define	POINT_H

#include "../Light.h"

namespace bx {

    namespace light {

        class Point : public Light {
        public:
            Point();
            ~Point();

            glm::vec3 getAttenuation();
            void setAttenuation(float constant, float linear, float quadric);
        private:
            glm::vec3 attenuation;
        };
    }
}


#endif	/* POINT_H */

