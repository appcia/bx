/* 
 * File:   Directional.cpp
 * Author: ciapunek
 * 
 * Created on August 12, 2012, 1:58 AM
 */

#include "Directional.h"

namespace bx {
    namespace light {

        Directional::Directional() {
            type = Light::Directional;
            setColor(0, 0, 0, 0);
        }

        Directional::Directional(const Directional& orig) {
        }

        Directional::~Directional() {
        }

        void Directional::setColor(float r, float g, float b, float alpha) {
            ambientColor = glm::vec4(r, g, b, alpha);
            diffuseColor = glm::vec4(r, g, b, alpha);
            specularColor = glm::vec4(r, g, b, alpha);
        }

        void Directional::setAmbientColor(float r, float g, float b, float alpha) {
            ambientColor = glm::vec4(r, g, b, alpha);
        }

        glm::vec4 Directional::getAmbientColor() {
            return ambientColor;
        }
    }
}


