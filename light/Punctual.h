/* 
 * File:   Punctual.h
 * Author: ciapunek
 *
 * Created on August 12, 2012, 1:54 AM
 */

#ifndef PUNCTUAL_H
#define	PUNCTUAL_H

#include "../Light.h"

namespace bx {
    namespace light {

        class Punctual : public Light {
        public:
            Punctual();
            Punctual(const Punctual& orig);
            virtual ~Punctual();

            glm::vec3 getAttenuation();
            void setAttenuation(float quadric, float linear, float constant);
        private:
            glm::vec3 attenuation;
        };
    }
}

#endif	/* PUNCTUAL_H */

