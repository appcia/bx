/* 
 * File:   Point.cpp
 * Author: hayalet
 *
 * Created on 11 sierpień 2012, 17:54
 */

#include "Point.h"
#include "../Light.h"

namespace bx {

    namespace light {

        Point::Point() {
            type = Point::PointLight;
            x, y, z = 0;
            setColor(0, 0, 0, 0);
            setAttenuation(0,0,0);
        }
        
        Point::~Point() {
            
        }

        void Point::setAttenuation(float constant, float linear, float quadric) {
            attenuation = glm::vec3(constant, linear, quadric);
        }
        
        glm::vec3 Point::getAttenuation() {
            return attenuation;
        }      
    }
}
