/* 
 * File:   Punctual.cpp
 * Author: ciapunek
 * 
 * Created on August 12, 2012, 1:54 AM
 */

#include "Punctual.h"

namespace bx {
    namespace light {

        Punctual::Punctual() {
            type = Light::Punctual;
            setColor(0, 0, 0, 0);
            setAttenuation(0, 0, 0);
        }

        Punctual::Punctual(const Punctual& orig) {
        }

        Punctual::~Punctual() {
        }

        void Punctual::setAttenuation(float quadric, float linear, float constant) {
            attenuation = glm::vec3(quadric, linear, constant);
        }

        glm::vec3 Punctual::getAttenuation() {
            return attenuation;
        }
    }
}



