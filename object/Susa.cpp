/* 
 * File:   Suzan.cpp
 * Author: ciapunek
 * 
 * Created on July 3, 2012, 10:51 PM
 */

#include "Susa.h"

namespace bx {

    namespace object {

        Susa::Susa() {
            name = "Susa";
            
            direction = 0;
            velocity = glm::vec2(0.0,0.0);

            moveAcceleration = 100;
            maxMovespeed = 10.0;
            groundDragFactor = 0.48;
            airDragFactor = 0.58;
            
            maxJumpTime = 0.35;
            jumpLaunchVelocity = -5.0;
            gravityAcceleration = 1.8;
            maxFallSpeed = 10;
            jumpControlPower = 0.01;           
        }

        bool Susa::load() {
            model = Model::loadObj("object/susa/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/susa/material.mtl");
            if (!material) return false;
            material->loadTexture("object/susa/texture.tga");

            shader = Shader::load("shader/phong_texture.vs", "shader/phong_texture.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }
    }


}

