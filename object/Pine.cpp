/* 
 * File:   Pine.h
 * Author: hayalet
 *
 * Created on 7 lipiec 2012, 19:19
 */

#include "Pine.h"

namespace bx {

    namespace object {

        Pine::Pine() {
            name = "Pine";
        }

        bool Pine::load() {
            model = Model::loadObj("object/pine/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/pine/material.mtl");
            if (!material) return false;
            material->loadTexture("object/pine/texture.tga");

            shader = Shader::load("shader/phong_color.vs", "shader/phong_color.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }
    }


}


