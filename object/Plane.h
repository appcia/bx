/* 
 * File:   Plane.h
 * Author: hayalet
 *
 * Created on 6 lipiec 2012, 23:35
 */

#ifndef PLANE_H
#define	PLANE_H

#include "../Object.h"
#include "../Scenery.h"

namespace bx {

    namespace object {

        class Plane : public Scenery {
        public:
            Plane();

            bool load();
        private:
        };
    }



}



#endif	/* PLANE_H */

