/* 
 * File:   Plane.h
 * Author: hayalet
 *
 * Created on 6 lipiec 2012, 23:35
 */

#ifndef SKYBOX_H
#define	SKYBOX_H

#include "../Object.h"
#include "../Scenery.h"

namespace bx {

    namespace object {

        class Skybox : public Scenery {
        public:
            Skybox();

            bool load();
        private:
        };
    }



}



#endif	/* SKYBOX_H */

