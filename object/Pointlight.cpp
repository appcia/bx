/* 
 * File:   Pointlight.h
 * Author: hayalet
 *
 * Created on 7 lipiec 2012, 11:24
 */


#include "Pointlight.h"

namespace bx {

    namespace object {

        Pointlight::Pointlight() {
            name = "Pointlight";
        }

        bool Pointlight::load() {
            model = Model::loadObj("object/pointlight/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/pointlight/material.mtl");
            if (!material) return false;
            material->loadTexture("object/susa/texture.tga");

            shader = Shader::load("shader/flatten_color.vs", "shader/flatten_color.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }

        void Pointlight::step(float time) {

        }
    }

}


