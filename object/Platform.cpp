/* 
 * File:   Platform.cpp
 * Author: hayalet
 * 
 * Created on 12 sierpień 2012, 21:19
 */

#include "Platform.h"

namespace bx {

    namespace object {

        Platform::Platform() {
            name = "Platform";
        }

        bool Platform::load() {
            model = Model::loadObj("object/platform/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/platform/material.mtl");
            if (!material) return false;
            material->loadTexture("object/platform/texture.tga");

            shader = Shader::load("shader/phong_texture.vs", "shader/phong_texture.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }
        
    }
}

