/* 
 * File:   Plane.cpp
 * Author: hayalet
 * 
 * Created on 6 lipiec 2012, 23:36
 */

#include "Skybox.h"

namespace bx {

    namespace object {

        Skybox::Skybox() {
            name = "Skybox";
        }

        bool Skybox::load() {
            model = Model::loadObj("object/skybox/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/skybox/material.mtl");
            if (!material) return false;
            material->loadTexture("object/skybox/texture.tga");

            shader = Shader::load("shader/flatten_texture.vs", "shader/flatten_texture.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }
    }

}


