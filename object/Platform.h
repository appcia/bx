/* 
 * File:   Platform.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 21:19
 */

#ifndef PLATFORM_H
#define	PLATFORM_H

#include "../Object.h"
#include "../Scenery.h"

namespace bx {

    namespace object {

        class Platform : public Scenery {
        public:
            Platform();

            bool load();
        private:

        };

    }
}

#endif	/* PLATFORM_H */

