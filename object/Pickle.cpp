/* 
 * File:   Pickle.cpp
 * Author: hayalet
 * 
 * Created on 12 sierpień 2012, 18:44
 */

#include "Pickle.h"

namespace bx {

    namespace object {

        Pickle::Pickle() {
            name = "Pickle";
        }

        bool Pickle::load() {
            model = Model::loadObj("object/pickle/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/pickle/material.mtl");
            if (!material) return false;
            material->loadTexture("object/pickle/texture.tga");

            shader = Shader::load("shader/phong_texture.vs", "shader/phong_texture.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }
    }
}

