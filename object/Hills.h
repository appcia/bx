/* 
 * File:   Hills.h
 * Author: hayalet
 *
 * Created on 7 lipiec 2012, 17:53
 */

#ifndef HILLS_H
#define	HILLS_H

#include "../Object.h"
#include "../Scenery.h"

namespace bx {

    namespace object {

        class Hills : public Scenery {
        public:
            Hills();

            bool load();
        private:
        };
    }

}

#endif	/* HILLS_H */

