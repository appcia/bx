/* 
 * File:   Suzan.h
 * Author: ciapunek
 *
 * Created on July 3, 2012, 10:51 PM
 */

#ifndef SUZAN_H
#define	SUZAN_H

#include "../Object.h"
#include "../Player.h"

namespace bx {

    namespace object {

        class Susa : public Player {
        public:
            Susa();

            bool load();
        private:
        };
    }



}

#endif	/* SUZAN_H */

