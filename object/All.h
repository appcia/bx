/* 
 * File:   All.h
 * Author: ciapunek
 *
 * Created on August 11, 2012, 8:20 PM
 */

#ifndef ALL_H
#define	ALL_H

#include "Hills.h"
#include "Pine.h"
#include "Plane.h"
#include "Pointlight.h"
#include "Skybox.h"
#include "Susa.h"

#endif	/* ALL_H */

