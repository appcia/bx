/* 
 * File:   Pointlight.h
 * Author: hayalet
 *
 * Created on 7 lipiec 2012, 11:24
 */

#ifndef POINTLIGHT_H
#define	POINTLIGHT_H

#include "../Object.h"

namespace bx {

    namespace object {

        class Pointlight : public Object {
        public:
            Pointlight();

            bool load();
            void step(float time);
        private:
        };
    }



}

#endif	/* POINTLIGHT_H */

