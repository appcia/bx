/* 
 * File:   Pine.h
 * Author: hayalet
 *
 * Created on 7 lipiec 2012, 19:19
 */

#ifndef PINE_H
#define	PINE_H

#include "../Object.h"
#include "../Scenery.h"

namespace bx {

    namespace object {

        class Pine : public Scenery {
        public:
            Pine();

            bool load();
        private:
        };
    }



}

#endif	/* PINE_H */

