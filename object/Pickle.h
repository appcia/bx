/* 
 * File:   Pickle.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 18:44
 */

#ifndef PICKLE_H
#define	PICKLE_H

#include "../Object.h"
#include "../Collectible.h"

namespace bx {
    namespace object {

        class Pickle : public Collectible {
        public:
            Pickle();

            bool load();
        private:

        };

    }
}


#endif	/* PICKLE_H */

