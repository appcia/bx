/* 
 * File:   EvilSusa.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 18:03
 */

#ifndef EVILSUSA_H
#define	EVILSUSA_H

#include "../Object.h"
#include "../Enemy.h"

namespace bx {
    namespace object {

        class EvilSusa : public Enemy {
        public:
            EvilSusa();

            bool load();
        private:

        };
    }
}



#endif	/* EVILSUSA_H */

