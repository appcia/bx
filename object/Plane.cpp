/* 
 * File:   Plane.cpp
 * Author: hayalet
 * 
 * Created on 6 lipiec 2012, 23:36
 */

#include "Plane.h"

namespace bx {

    namespace object {

        Plane::Plane() {
            name = "Plane";
        }

        bool Plane::load() {
            model = Model::loadObj("object/plane/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/plane/material.mtl");
            if (!material) return false;
            material->loadTexture("object/plane/texture.tga");

            shader = Shader::load("shader/phong_texture.vs", "shader/phong_texture.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }
    }


}


