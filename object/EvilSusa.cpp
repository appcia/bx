/* 
 * File:   EvilSusa.cpp
 * Author: hayalet
 * 
 * Created on 12 sierpień 2012, 18:03
 */

#include "EvilSusa.h"

namespace bx {

    namespace object {

        EvilSusa::EvilSusa() {
            name = "EvilSusa";
            
            direction = 0;
            velocity = glm::vec2(0.0,0.0);

            moveAcceleration = 100;
            maxMovespeed = 10.0;
            groundDragFactor = 0.48;
            airDragFactor = 0.58;
        }

        bool EvilSusa::load() {
            model = Model::loadObj("object/evilsusa/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/evilsusa/material.mtl");
            if (!material) return false;
            material->loadTexture("object/evilsusa/texture.tga");

            shader = Shader::load("shader/phong_texture.vs", "shader/phong_texture.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }
    }
}
