/* 
 * File:   Plane.cpp
 * Author: hayalet
 * 
 * Created on 7 lipiec 2012, 18:05
 */

#include "Hills.h"

namespace bx {

    namespace object {

        bool Hills::load() {
            model = Model::loadObj("object/hills/model.obj");
            if (!model) return false;

            material = Material::loadMtl("object/hills/material.mtl");
            if (!material) return false;
            material->loadTexture("object/hills/texture.tga");

            shader = Shader::load("shader/phong_texture.vs", "shader/phong_texture.fs", "");
            if (!shader || shader->isError()) return false;

            return true;
        }

        Hills::Hills() {
            name = "Hills";
        }
    }
}

