/* 
 * File:   App.h
 * Author: ciapunek
 *
 * Created on June 4, 2012, 1:20 PM
 */

#ifndef APP_H
#define	APP_H

#include "Scene.h"
#include "Storage.h"
#include "Config.h"

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>
#include <cstdio>

using namespace std;

namespace bx {

    class App {
    public:
        static App * instance();
        virtual ~App();
        
        int exec();
        
        Config * getConfig();
        Storage * getStorage();
        Scene * getScene();
        
        sf::Clock * getClock();
        sf::RenderWindow * getWindow();
        
        bool isDebugMode();
        
    private:
        static App * singleton;
        App();
        App & operator=(const App&) {};
        
        Config * config;
        Storage * storage;
        Scene * scene;
        
        sf::RenderWindow * window;
        sf::Clock * clock;
        
        bool debugMode;
        int width;
        int height;
    };
}



#endif	/* APP_H */

