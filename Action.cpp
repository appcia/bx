/* 
 * File:   Action.cpp
 * Author: ciapunek
 * 
 * Created on August 7, 2012, 10:57 PM
 */

#include "Action.h"

namespace bx {

    Action::Action() {
        name = "Action";
        complete = false;
        repeatable = true;
    }

    Action::~Action() {
    }

    string Action::getName() {
        return name;
    }

    Action::Priority Action::getPriority() {
        return priority;
    }

    void Action::setComplete() {
        complete = true;
    }

    bool Action::isComplete() {
        return complete;
    }
    
    bool Action::isRepeatable() {
        return repeatable;
    }

    void Action::setActor(Actor * actor) {
        this->actor = actor;
    }

    Actor * Action::getActor(Actor* actor) {
        return actor;
    }
}
