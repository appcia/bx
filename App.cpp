/* 
 * File:   App.cpp
 * Author: ciapunek
 * 
 * Created on June 4, 2012, 1:20 PM
 */

#include "App.h"
#include "action/Turnaround.h"

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <iostream>
#include <map>
#include <string>
#include <cmath>

#include "action/Demo.h"
#include "action/Move.h"
#include "action/Die.h"
#include "action/Turnaround.h"
#include "action/Jump.h"
#include "action/Stop.h"

namespace bx {

    // Singleton initialization
    App * App::singleton = 0;

    App * App::instance() {
        if (!singleton) {
            singleton = new App();
        }
        return singleton;
    }

    App::App() {
        clock = new sf::Clock;

        debugMode = false;
    }

    App::~App() {
        delete singleton;

        if (config) delete config;
        if (storage) delete storage;
        if (scene) delete scene;

        if (clock) delete clock;
        if (window) delete window;
    }

    Config * App::getConfig() {
        return config;
    }

    Storage * App::getStorage() {
        return storage;
    }

    Scene * App::getScene() {
        return scene;
    }

    sf::Clock * App::getClock() {
        return clock;
    }

    sf::RenderWindow * App::getWindow() {
        return window;
    }

    int App::exec() {

        // Init configuration, storage, window and then scene
        Util::debug("Application started");

        config = Config::loadDefault();
        Util::debug("Configuration loaded");

        debugMode = config->getInt("debugMode");

        storage = new Storage();
        Util::debug("Storage created");

        width = config->getInt("sceneWidth");
        height = config->getInt("sceneHeight");

        sf::WindowSettings settings;
        settings.AntialiasingLevel = config->getInt("antialiasingLevel");
        settings.DepthBits = config->getInt("depthBits");
        settings.StencilBits = config->getInt("stencilBits");

        window = new sf::RenderWindow(sf::VideoMode(width, height), config->get("windowTitle"), config->getInt("windowStyle"), settings);
        window->PreserveOpenGLStates(true);
        window->SetFramerateLimit(config->getInt("framerateLimit"));
        window->UseVerticalSync(config->getInt("verticalSync"));
        Util::debug("Window created");

        scene = new bx::Scene;
        if (!scene->init()) {
            Util::debug("Could not initialize scene");
            return EXIT_FAILURE;
        }
        scene->resize(width, height);
        Util::debug("Scene created");

        // Statistics
        Util::debug("Storage statistics");
        Util::print("* hits %d\n", (int) storage->getHits());
        Util::print("* misses %d\n", (int) storage->getMisses());
        Util::print("* resources %d\n", (int) storage->getItemCount());
        Util::print("* instances %d\n", (int) storage->getInstanceCount());

        // Game loop
        Util::debug("Starting game loop...");
        bool started = false;
        float lastTime = clock->GetElapsedTime();

        sf::Event event;
        while (window->IsOpened()) {

            // Time measuring
            const float currentTime = clock->GetElapsedTime();
            const float deltaTime = float(currentTime - lastTime);
            const float fps = 1.f / deltaTime;
            lastTime = currentTime;

            // Dispatch window events
            while (window->GetEvent(event)) {

                // Closing window
                if (event.Type == sf::Event::Closed) {
                    window->Close();
                }

                // Resizing window
                if (event.Type == sf::Event::Resized) {
                    scene->resize(event.Size.Width, event.Size.Height);
                }

                // Global keybinding scope
                if (event.Type == sf::Event::KeyPressed) {
                    switch (event.Key.Code) {
                        case sf::Key::Escape :
                            // Close window
                            window->Close();
                            break;
                        case sf::Key::F11 :
                            //Change camera mode
                            debugMode = !debugMode;
                            break;
                        case sf::Key::F3:
                            // Screenshot
                            window->Capture().SaveToFile("capture.jpg");
                            Util::debug("Screenshot captured and saved to file 'capture.jpg'");
                            break;
                    }
                }

                // Debug mode (camera moved by cursor)
                if (debugMode) {
                    window->ShowMouseCursor(false);

                    if (event.Type == sf::Event::MouseMoved) {
                        const int posX = event.MouseMove.X;
                        const int posY = event.MouseMove.Y;
                        const float speed = config->getFloat("mouseMoveSpeed") / 1000;

                        // Compute new orientation
                        float hAngleDelta = speed * deltaTime * float(width / 2 - posX);
                        float vAngleDelta = speed * deltaTime * float(height / 2 - posY);

                        scene->getCamera()->rotate(hAngleDelta, vAngleDelta);

                        // Restore mouse (to calculate differences)
                        window->SetCursorPosition(width / 2, height / 2);
                    }

                    {
                        const float speed = config->getFloat("cameraMoveSpeed");

                        // Debug keybinding scope
                        if (event.Type == sf::Event::KeyPressed) {
                            switch (event.Key.Code) {
                                case sf::Key::Up:
                                    scene->getCamera()->moveUp(deltaTime * speed);
                                    break;
                                case sf::Key::Down:
                                    scene->getCamera()->moveDown(deltaTime * speed);
                                    break;
                                case sf::Key::Left:
                                    scene->getCamera()->moveLeft(deltaTime * speed);
                                    break;
                                case sf::Key::Right:
                                    scene->getCamera()->moveRight(deltaTime * speed);
                                    break;
                            }
                        }
                    }
                }// Game mode (camera follows player)
                else {
                    window->ShowMouseCursor(true);

                    Action * action = scene->getPlayer()->getCurrentAction();

                    // Game keybinding scope
                    if (event.Type == sf::Event::KeyPressed) {
                        switch (event.Key.Code) {
                            case sf::Key::Left:
                                if (scene->getPlayer()->getDirection() != -1) {
                                    scene->getPlayer()->addAction(new action::Turnaround(action::Turnaround::Left));
                                    scene->getPlayer()->addAction(new action::Move(action::Move::Left));
                                }
                                break;
                            case sf::Key::Right:
                                if (scene->getPlayer()->getDirection() != 1) {
                                    scene->getPlayer()->addAction(new action::Turnaround(action::Turnaround::Right));
                                    scene->getPlayer()->addAction(new action::Move(action::Move::Right));
                                }

                                break;
                            case sf::Key::Space:
                                scene->getPlayer()->addAction(new action::Jump());
                                break;
                            case sf::Key::Num1:
                                scene->getPlayer()->addAction(new action::Demo());
                                break;
                            case sf::Key::Num2:
                                scene->getPlayer()->addAction(new action::Turnaround(action::Turnaround::Right));
                                break;
                            case sf::Key::Num9:
                                scene->getPlayer()->addAction(new action::Die());
                                break;
                            case sf::Key::Num0:
                                if (action) {
                                    action->setComplete();
                                }
                                break;
                        }
                    } else if (event.Type == sf::Event::KeyReleased) {
                        switch (event.Key.Code) {
                            case sf::Key::Left:
                                if (action && action->getName() == "Move") {
                                    action->setComplete();
                                    scene->getPlayer()->removeAllActions();
                                }
                                scene->getPlayer()->addAction(new action::Turnaround(action::Turnaround::Right));
                                scene->getPlayer()->addAction(new action::Stop());
                                break;
                            case sf::Key::Right:
                                if (action && action->getName() == "Move") {
                                    action->setComplete();
                                    scene->getPlayer()->removeAllActions();
                                }
                                scene->getPlayer()->addAction(new action::Turnaround(action::Turnaround::Left));
                                scene->getPlayer()->addAction(new action::Stop());
                                break;
                        }
                    }
                }
            }

            if (!debugMode) {
                scene->getCamera()->follow(scene->getPlayer(), 20.0, 4.0);
            }

            scene->getCamera()->moveCamera();

            scene->step(deltaTime);
            scene->draw();

            // It is necessary to call Display() method of sf::RenderWindow,
            // otherwise the buffers will not be swapped.
            window->Display();

            if (!started) {
                started = true;
                Util::debug("Rendering...");
            }
        }

        Util::debug("Application ended");

        return EXIT_SUCCESS;
    }
}
