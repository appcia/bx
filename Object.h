/* 
 * File:   Object.h
 * Author: ciapunek
 *
 * Created on July 3, 2012, 10:48 PM
 */

#ifndef OBJECT_H
#define	OBJECT_H

#include "Point.h"
#include "Model.h"
#include "Material.h"
#include "Shader.h"
#include "BoundingBox.h"
#include "Projector.h"
#include "Camera.h"

#include "Gl.h"

namespace bx {

    class Projector;
    class Camera;
    class Shader;
    class Model;

    class Object : public Point {
    public:

        Object();
        virtual ~Object();

        virtual bool load() = 0;
        
        void init();

        string getName();

        Model * getModel();
        Material * getMaterial();

        void setShader(Shader * shader);
        Shader * getShader();
        
        glm::mat4 getMatrix();

        void upload();
        void draw(Projector * projector, Camera * camera);

        glm::vec3 getRotation();

        glm::vec3 getScale();
        void setScale(float scaleX, float scaleY, float scaleZ);
        void setScale(float scale);
        
        bool isRenderable();

        bool isVisible();
        void hide();
        void show();
        
        void setScaleZ(float scaleZ);
        float getScaleZ() const;
        void setScaleY(float scaleY);
        float getScaleY() const;
        void setScaleX(float scaleX);
        float getScaleX() const;
        
        void setRotationX(float rotX);
        float getRotationX() const;
        void setRotationZ(float rotZ);
        float getRotationZ() const;
        void setRotationY(float rotY);
        float getRotationY() const;
        
        static bool check2Dcollisions(Object * object1, Object * object2);
    protected:
        string name;

        bool renderable;
        bool visible;

        float scaleX, scaleY, scaleZ;
        float rotationX, rotationY, rotationZ;

        Model * model;
        Material * material;
        Shader * shader;
        BoundingBox * boundingBox;

        glm::mat4 identityMatrix;
    };
}

#endif	/* OBJECT_H */

