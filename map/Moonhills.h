/* 
 * File:   Moonhills.h
 * Author: ciapunek
 *
 * Created on August 11, 2012, 1:12 PM
 */

#ifndef MOONHILLS_H
#define	MOONHILLS_H

#include "../Map.h"

namespace bx {

    namespace map {

        class Moonhills : public Map {
        public:
            Moonhills();

            void use(Scene * scene);
        private:

        };
    }

}


#endif	/* MOONHILLS_H */

