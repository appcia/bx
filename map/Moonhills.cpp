/* 
 * File:   Moonhills.cpp
 * Author: ciapunek
 * 
 * Created on August 11, 2012, 1:12 PM
 */

#include "Moonhills.h"
#include "../Gl.h"

namespace bx {

    namespace map {

        Moonhills::Moonhills() {
            name = "Moonhills";
        }

        void Moonhills::use(Scene * scene) {

            // Objects
            object::Susa * susa = new object::Susa();
            susa->load();
            susa->upload();
            susa->setScale(0.7);
            susa->setPosition(-5.0, -1.2, -1.0);
            scene->addActor(susa);

            scene->setPlayer(susa);
            
            object::EvilSusa * enemy1 = new object::EvilSusa();
            enemy1->load();
            enemy1->upload();
            enemy1->setScale(0.7);
            enemy1->setPosition(4.0, 2 , -1.0);
            scene->addActor(enemy1);
            
            object::EvilSusa * enemy2 = new object::EvilSusa();
            enemy2->load();
            enemy2->upload();
            enemy2->setScale(0.7);
            enemy2->setPosition(-4.0, 9 , -1.0);
            scene->addActor(enemy2);
            
            //enemy1->addAction(new action::Move(action::Move::Left));
            
            object::Pickle * pickle1 = new object::Pickle();
            pickle1->load();
            pickle1->upload();
            pickle1->setScale(0.15);
            pickle1->setPosition(-5.0, 4, -1.0);
            scene->addActor(pickle1);
            
            pickle1->addAction(new action::Float());
            
            object::Pickle * pickle2 = new object::Pickle();
            pickle2->load();
            pickle2->upload();
            pickle2->setScale(0.15);
            pickle2->setPosition(4.0, 6, -1.0);
            scene->addActor(pickle2);
            
            pickle2->addAction(new action::Float());
            
            object::Pickle * pickle3 = new object::Pickle();
            pickle3->load();
            pickle3->upload();
            pickle3->setScale(0.15);
            pickle3->setPosition(3.0, -1.5, -1.0);
            scene->addActor(pickle3);
            
            pickle3->addAction(new action::Float());

            object::Skybox * skybox = new object::Skybox();
            skybox->load();
            skybox->upload();
            skybox->setPosition(0.0, 0.0, 0.0);
            skybox->setRotationY(180.0f);
            skybox->setScale(1.0);
            scene->addScenery(skybox);

            object::Plane * floor = new object::Plane();
            floor->load();
            floor->upload();
            floor->setPosition(0.0, -2.0, 0.0);
            floor->setScale(1.1);
            scene->addScenery(floor);
            
            object::Platform * platform1 = new object::Platform();
            platform1->load();
            platform1->upload();
            platform1->setPosition(3.0,1.0, 0.0);
            platform1->setScale(3,1,1);
            scene->addScenery(platform1);
            
            object::Platform * platform2 = new object::Platform();
            platform2->load();
            platform2->upload();
            platform2->setPosition(-5.0,3.0, 0.0);
            platform2->setScale(2,1,1);
            scene->addScenery(platform2);
            
            object::Platform * platform3 = new object::Platform();
            platform3->load();
            platform3->upload();
            platform3->setPosition(0.0,5.0, 0.0);
            platform3->setScale(1,1,1);
            scene->addScenery(platform3);
            
            object::Platform * platform4 = new object::Platform();
            platform4->load();
            platform4->upload();
            platform4->setPosition(4.0,5.0, 0.0);
            platform4->setScale(1.5,1,1);
            scene->addScenery(platform4);
            
            object::Platform * platform5 = new object::Platform();
            platform5->load();
            platform5->upload();
            platform5->setPosition(-4.0,8.0, 0.0);
            platform5->setScale(3,1,1);
            scene->addScenery(platform5);

            object::Hills * hills = new object::Hills();
            hills->load();
            hills->upload();
            hills->setPosition(-2.0, -2.0, -7.0);
            scene->addScenery(hills);

            object::Hills * hills2 = new object::Hills();
            hills2->load();
            hills2->upload();
            hills2->setPosition(2.0, -2.0, -10.0);
            hills2->setScale(0.7);
            hills2->setRotationY(-45.0f);
            scene->addScenery(hills2);

            object::Pine * pine = new object::Pine();
            pine->load();
            pine->upload();
            pine->setPosition(-6.0, 0.5, -7.2);
            pine->setScale(0.2);
            scene->addScenery(pine);

            object::Pine * pine2 = new object::Pine();
            pine2->load();
            pine2->upload();
            pine2->setPosition(-4.0, -0.9, -6.7);
            pine2->setRotationX(30.0f);
            pine2->setRotationZ(-10.0f);
            pine2->setScale(0.2, 0.3, 0.2);
            scene->addScenery(pine2);

            object::Pine * pine3 = new object::Pine();
            pine3->load();
            pine3->upload();
            pine3->setScale(0.2);
            pine3->setRotationX(-15.0f);
            pine3->setRotationZ(-15.0f);
            pine3->setPosition(-3.0, -0.9, -7.2);
            scene->addScenery(pine3);

            object::Pine * pine4 = new object::Pine();
            pine4->load();
            pine4->upload();
            pine4->setPosition(-5.5, -0.7, -6.5);
            pine4->setRotationX(35.0f);
            pine4->setScale(0.15, 0.23, 0.2);
            scene->addScenery(pine4);

            object::Pine * pine5 = new object::Pine();
            pine5->load();
            pine5->upload();
            pine5->setPosition(-7.5, 0.2, -6.7);
            pine5->setRotationX(15.0f);
            pine5->setRotationZ(10.0f);
            pine5->setScale(0.12, 0.15, 0.12);
            scene->addScenery(pine5);

            object::Pine * pine6 = new object::Pine();
            pine6->load();
            pine6->upload();
            pine6->setPosition(4.0, -0.2, -7.2);
            pine6->setScale(0.2);
            scene->addScenery(pine6);

            object::Pine * pine7 = new object::Pine();
            pine7->load();
            pine7->upload();
            pine7->setPosition(2.5, -0.3, -6.7);
            pine7->setRotationX(10.0f);
            pine7->setScale(0.2, 0.3, 0.2);
            scene->addScenery(pine7);

            object::Pine * pine8 = new object::Pine();
            pine8->load();
            pine8->upload();
            pine8->setScale(0.2);
            pine8->setRotationX(-15.0f);
            pine8->setRotationZ(15.0f);
            pine8->setPosition(1.0, -0.7, -7.2);
            scene->addScenery(pine8);

            object::Pine * pine9 = new object::Pine();
            pine9->load();
            pine9->upload();
            pine9->setPosition(3.2, -0.5, -6);
            pine9->setRotationX(35.0f);
            pine9->setScale(0.15, 0.23, 0.2);
            scene->addScenery(pine9);

            object::Pine * pine10 = new object::Pine();
            pine10->load();
            pine10->upload();
            pine10->setPosition(0, -0.4, -12);
            pine10->setRotationX(15.0f);
            pine10->setRotationZ(10.0f);
            pine10->setScale(0.12, 0.15, 0.12);
            scene->addScenery(pine10);

            object::Pine * pine11 = new object::Pine();
            pine11->load();
            pine11->upload();
            pine11->setPosition(-1, -1.5, -10.8);
            pine11->setScale(0.12, 0.15, 0.12);
            scene->addScenery(pine11);

            // Lights
            light::Directional * moon = new light::Directional();
            moon->setPosition(10, -10, -10);
            moon->setAmbientColor(0.3, 0.3, 0.4, 1);
            moon->setDiffuseColor(0.5, 0.5, 0.8, 1);
            moon->setSpecularColor(0.5, 0.5, 0.8, 1);
            scene->addLight(moon);

            light::Punctual * light1 = new light::Punctual();
            light1->setPosition(-6, 1, 0);
            light1->setColor(0.8, 0.5, 0, 1);
            light1->setAttenuation(0.2, 0.1, 0.01);
            scene->addLight(light1);

            light::Punctual * light2 = new light::Punctual();
            light2->setPosition(5, 4, 0);
            light2->setColor(0.8, 0.5, 0, 1);
            light2->setAttenuation(0.2, 0.1, 0.1);
            scene->addLight(light2);

            light::Punctual * light3 = new light::Punctual();
            light3->setPosition(-1, 1, -8);
            light3->setColor(0.6, 0.6, 0.8, 1);
            light3->setAttenuation(0.2, 0.1, 0.08);
            scene->addLight(light3);
            
            light::Punctual * light4 = new light::Punctual();
            light4->setPosition(-3, 7, 0);
            light4->setColor(0, 0.5, 0, 1);
            light4->setAttenuation(0.1, 0.1, 0.2);
            scene->addLight(light4);
        }
    }
}


