/* 
 * File:   Storage.cpp
 * Author: ciapunek
 * 
 * Created on July 7, 2012, 1:47 PM
 */

#include "Common.h"
#include "Storage.h"
#include "Resource.h"

#include <vector>

namespace bx {

    Storage::Storage() {
    }

    Storage::Storage(const Storage& orig) {
    }

    Storage::~Storage() {
        for (int i = 0; i < items.size(); i++) {
            if (items[i]) {
                if (items[i]->resource) {
                    delete items[i]->resource;
                }
                delete items[i];
            }
        }
    }

    bool Storage::addAsReference(Resource * resource, int hash) {
        if (hash != 0 && !hasResource(hash)) {
            resource->setHash(hash);
            
            Item * item = new Item;
            item->resource = resource;
            item->reference = true;
            
            items.push_back(item);
            return true;
        }
        return false;
    }
    
    Resource * Storage::addAsInstance(Resource * resource, int hash) {
        if (hash != 0 && !hasResource(hash)) {
            resource->setHash(hash);
            
            Item * item = new Item;
            item->resource = resource;
            item->reference = false;
            
            items.push_back(item);
            
            Resource * instance = resource->clone();
            instances.push_back(instance);
            
            return instance;
        }
        return NULL;
    }

    bool Storage::hasResource(int hash) {
        if (hash != 0) {
            for (int i = 0; i < items.size(); i++) {
                if (items[i]->resource->getHash() == hash) {
                    return true;
                }
            }
        }
        return false;
    }

    Resource * Storage::getResource(int hash) {
        if (hash != 0) {
            for (int i = 0; i < items.size(); i++) {
                if (items[i]->resource->getHash() == hash) {
                    hits++;
                    
                    if (items[i]->reference) {
                        return items[i]->resource;
                    }
                    else {
                        Resource * instance = items[i]->resource->clone();
                        instances.push_back(instance);
                        return instance;
                    }
                }
            }
        }

        misses++;
        return NULL;
    }

    uint Storage::getItemCount() {
        return items.size();
    }
    
    uint Storage::getInstanceCount() {
        return instances.size();
    }

    uint Storage::getHits() {
        return hits;
    }

    uint Storage::getMisses() {
        return misses;
    }
}


