/* 
 * File:   Gl.h
 * Author: ciapunek
 *
 * Created on July 4, 2012, 9:42 PM
 */

#ifndef GL_H
#define	GL_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_operation.hpp>

#endif	/* GL_H */

