/* 
 * File:   Projector.h
 * Author: ciapunek
 *
 * Created on June 18, 2012, 5:40 PM
 */

#ifndef PROJECTOR_H
#define	PROJECTOR_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace bx {

    class Projector {
    public:
        float aspect;
        float zNear;
        float fowY;
        float zFar;

        Projector();
        Projector(const Projector& orig);
        virtual ~Projector();

        void setDefaults();
        void setFowY(float degrees);
        
        glm::mat4 getMatrix();

    private:
    };
}


#endif	/* PROJECTOR_H */

