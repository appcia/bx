/* 
 * File:   Enemy.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 17:57
 */

#ifndef ENEMY_H
#define	ENEMY_H

#include "Object.h"
#include "Actor.h"

namespace bx {

    class Enemy : public Actor {
    public:
        Enemy();
        Enemy(const Enemy& orig);
        virtual ~Enemy();
    private:

    };
}

#endif	/* ENEMY_H */

