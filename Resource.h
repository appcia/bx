/* 
 * File:   Resource.h
 * Author: ciapunek
 *
 * Created on July 7, 2012, 1:38 PM
 */

#ifndef RESOURCE_H
#define	RESOURCE_H

#include <string>

using namespace std;

namespace bx {

    class Resource {
    public:
        Resource();
        Resource(const Resource& orig);
        virtual ~Resource();
        
        virtual Resource * clone() = 0;

        static int generateHash(string value);
        virtual int getHash();
        virtual void setHash(int hash);

    protected:
        string type;
        int hash;
    };
}


#endif	/* RESOURCE_H */

