/* 
 * File:   Texture.h
 * Author: ciapunek
 *
 * Created on July 5, 2012, 8:29 PM
 */

#ifndef TEXTURE_H
#define	TEXTURE_H

#include "Resource.h"

#include "Gl.h"
#include "vendor/Tga.h"

#include <string>

using namespace std;

namespace bx {

    class Texture : public Resource {
    public:
        Texture();
        virtual ~Texture();

        Texture * clone() {
            return new Texture(*this);
        }

        GLint getId();
        string getPath();

        static Texture * loadTga(string path);

        void upload();
        bool isUploaded();

        void start();
        void stop();
    private:
        GLuint id;
        string path;

        uint width;
        uint height;
        uint bpp;
        uint size;

        unsigned char * data;
        bool uploaded;
    };
}



#endif	/* TEXTURE_H */

