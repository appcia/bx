/* 
 * File:   Collectible.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 17:38
 */

#ifndef COLLECTIBLE_H
#define	COLLECTIBLE_H

#include "Object.h"
#include "Actor.h"

namespace bx {
    class Collectible : public Actor {
    public:
        Collectible();
        Collectible(const Collectible& orig);
        virtual ~Collectible();
        
        int getValue();
    private:
        int value;
    };

}

#endif	/* COLLECTIBLE_H */

