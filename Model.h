/* 
 * File:   Model.h
 * Author: ciapunek
 *
 * Created on June 12, 2012, 8:06 PM
 */

#ifndef MODEL_H
#define	MODEL_H

#include "BoundingBox.h"
#include "Shader.h"
#include "Gl.h"

#include <vector>
#include <string>

using namespace std;

namespace bx {

    class Shader;
    
    class Model {
    public:
        Model();
        Model(const Model& orig);
        virtual ~Model();

        static Model * loadObj(string path);
        
        void init();

        void upload(Shader * shader);
        bool isUploaded();
        
        void send(Object * object, Projector * projector, Camera * camera);
        void draw();
        
        const vector <glm::vec3> & getVertices();

        string getFile();
    private:
        string file;
        
        bool uploaded;

        vector <glm::vec3> vertices;
        vector <glm::vec2> uvs;
        vector <glm::vec3> normals;

        GLuint vboVertices;
        GLuint vboNormals;
        GLuint vboUvs;

        GLint locVertex;
        GLint locNormal;
        GLint locUv;
    };
}


#endif	/* MODEL_H */

