/* 
 * File:   Map.h
 * Author: ciapunek
 *
 * Map object (contains all models and neccessary data)
 * 
 * Created on June 8, 2012, 4:26 PM
 */

#ifndef MAP_H
#define	MAP_H

#include "Scene.h"

#include "light/Directional.h"
#include "light/Punctual.h"

#include "object/Susa.h"
#include "object/Plane.h"
#include "object/Skybox.h"
#include "object/Pointlight.h"
#include "object/Hills.h"
#include "object/Pine.h"
#include "object/EvilSusa.h"
#include "object/Pickle.h"
#include "object/Platform.h"

#include "action/Demo.h"
#include "action/Float.h"

#include <string>

namespace bx {

    class Scene;

    class Map {
    public:
        Map();
        virtual ~Map();

        string getName();

        static Map * load(string filename);
        virtual void use(Scene * scene);

    protected:
        std::string name;
    };
}


#endif	/* MAP_H */

