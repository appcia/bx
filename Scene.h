/* 
 * File:   Scene.h
 * Author: ciapunek
 *
 * Created on June 4, 2012, 1:37 PM
 */

#ifndef SCENE_H
#define	SCENE_H

#include "Gl.h"
#include "Projector.h"
#include "Camera.h"
#include "Object.h"
#include "Light.h"
#include "Actor.h"
#include "Scenery.h"
#include "Player.h"
#include "light/Directional.h"

#include <vector>

using namespace std;

namespace bx {
    
    class Map;
    class Light;
    
    class Scene {
    public:
        Scene();
        virtual ~Scene();

        Camera * getCamera();
        Projector * getProjector();

        bool init();
        void resize(int width, int height);
        void step(float Time);
        
        void draw();
        void drawInit();
        void drawObjects();

        // Object management
        bool removeObject(Object * object);
        void addScenery(Scenery * scenery);
        void addActor(Actor * actor);

        // Light management
        void addLight(Light * light);
        bool removeLight(Light * light);
        void attachLights(Shader * shader);
        
        // Game
        void setPlayer(Player * player);
        Player * getPlayer();
    private:
        void addObject(Object * object);
        
        GLint width;
        GLint height;
        GLfloat aspect;

        Projector * projector;
        Camera * camera;
        Map * map;

        vector<Object *> objects;
        vector<Actor *> actors;
        vector<Scenery *> sceneries;
        Player * player;
        vector<Light *> lights;

        // Scene UBO
        GLuint lightsBuffer;
    };
}



#endif	/* SCENE_H */

