/* 
 * File:   Camera.h
 * Author: ciapunek
 *
 * Created on June 8, 2012, 2:10 PM
 */

#ifndef CAMERA_H
#define	CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace bx {

    class Object;
    
    class Camera {
    public:
        Camera();
        Camera(const Camera& orig);
        virtual ~Camera();

        glm::mat4 getMatrix();

        void setDefaults();

        void lookAt(glm::vec3 eye, glm::vec3 direction, glm::vec3 up);
        void follow(Object * object, float zoom, float height);

        void moveUp(float delta);
        void moveDown(float delta);
        void moveLeft(float delta);
        void moveRight(float delta);
        void rotate(float horizontalAngle, float verticalAngle);
        
        void moveCamera();

    private:
        glm::mat4 matrix;

        glm::vec3 position;
        glm::vec3 direction;
        glm::vec3 up;

        float hAngle;
        float vAngle;
    };
}

#endif	/* CAMERA_H */

