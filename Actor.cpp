/* 
 * File:   Actor.cpp
 * Author: ciapunek
 * 
 * Created on August 7, 2012, 10:54 PM
 */

#include "Actor.h"
#include "Util.h"

namespace bx {

    /**
     * Priority queue ordering rule
     */
    bool ActionCompare::operator()(Action * a1, Action * a2) {
        return a1->getPriority() < a2->getPriority();
    }

    Actor::Actor() {
        action = 0;
        idle = true;
    }

    Actor::Actor(const Actor& orig) {
    }

    Actor::~Actor() {
        if (action) delete action;

        while (!actionQueue.empty()) {
            Action * action = actionQueue.top();
            if (action) delete action;
            actionQueue.pop();
        }
    }

    /**
     * Resolve actor collisions with scenery and other actors
     */
    void Actor::resolve() {
        // @todo Use BoundingBox class
    }

    /**
     * Actor is not busy (not doing some action)
     * 
     * @return bool
     */
    bool Actor::isIdle() {
        return idle;
    }

    void Actor::setJumpControlPower(float jumpControlPower) {
        this->jumpControlPower = jumpControlPower;
    }

    float Actor::getJumpControlPower() const {
        return jumpControlPower;
    }

    void Actor::setMaxFallSpeed(float maxFallSpeed) {
        this->maxFallSpeed = maxFallSpeed;
    }

    float Actor::getMaxFallSpeed() const {
        return maxFallSpeed;
    }

    void Actor::setGravityAcceleration(float gravityAcceleration) {
        this->gravityAcceleration = gravityAcceleration;
    }

    float Actor::getGravityAcceleration() const {
        return gravityAcceleration;
    }

    void Actor::setJumpLaunchVelocity(float jumpLaunchVelocity) {
        this->jumpLaunchVelocity = jumpLaunchVelocity;
    }

    float Actor::getJumpLaunchVelocity() const {
        return jumpLaunchVelocity;
    }

    void Actor::setMaxJumpTime(float maxJumpTime) {
        this->maxJumpTime = maxJumpTime;
    }

    float Actor::getMaxJumpTime() const {
        return maxJumpTime;
    }

    void Actor::setAirDragFactor(float airDragFactor) {
        this->airDragFactor = airDragFactor;
    }

    float Actor::getAirDragFactor() const {
        return airDragFactor;
    }

    void Actor::setGroundDragFactor(float groundDragFactor) {
        this->groundDragFactor = groundDragFactor;
    }

    float Actor::getGroundDragFactor() const {
        return groundDragFactor;
    }

    void Actor::setMaxMovespeed(float maxMovespeed) {
        this->maxMovespeed = maxMovespeed;
    }

    float Actor::getMaxMovespeed() const {
        return maxMovespeed;
    }

    void Actor::setMoveAcceleration(float moveAcceleration) {
        this->moveAcceleration = moveAcceleration;
    }

    float Actor::getMoveAcceleration() const {
        return moveAcceleration;
    }

    void Actor::setVelocity(glm::vec2 velocity) {
        this->velocity = velocity;
    }
    
    void Actor::setVelocityX(float velocity) {
        this->velocity.x = velocity;
    }
    
    void Actor::setVelocityY(float velocity) {
        this->velocity.y = velocity;
    }

    glm::vec2 Actor::getVelocity() const {
        return velocity;
    }

    void Actor::setDirection(int direction) {
        this->direction = direction;
    }

    int Actor::getDirection() const {
        return direction;
    }

    /**
     * Step current action if any
     * If completed, fetch next from priority queue
     * 
     * @param time
     */
    void Actor::step(float time) {
        update();

        if (!action) {
            action = getNextAction();
        }

        if (action && action->isComplete()) {
            Util::debug("Actor " + getName() + " completed action " + action->getName());
            
            delete action;
            action = getNextAction();
        }

        if (action) {
            action->step(time);
            idle = false;
        } else {
            idle = true;
        }
    }

    bool Actor::addAction(Action * action) {
        if (this->action) {
            if ((this->action->getName() == action->getName()) && !this->action->isRepeatable()) {
                return false;
            }
        }

        action->setActor(this);
        actionQueue.push(action);

        Util::debug("Actor " + getName() + " will do " + action->getName());
        return true;
    }
    
    Action * Actor::getNextAction() {
        if (!actionQueue.empty()) {
            Action * action = actionQueue.top();
            actionQueue.pop();
            
            Util::debug("Actor " + getName() + " is now doing " + action->getName());
            return action;
        }
        
        return NULL;
    }

    void Actor::update() {
        this->boundingBox->transform(identityMatrix);
        this->boundingBox->get2DRectangle();
    }

    /**
     * Current action
     */
    Action * Actor::getCurrentAction() {
        return action;
    }

    /**
     * Actions left to do
     */
    uint Actor::getActionsLeft() {
        return actionQueue.size();
    }
    
    void Actor::removeAllActions() {
        while(!actionQueue.empty()){
           actionQueue.pop(); 
        }
    }
}

