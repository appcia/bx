/* 
 * File:   Shader.h
 * Author: ciapunek
 *
 * Created on June 9, 2012, 11:37 PM
 */

#ifndef SHADER_H
#define	SHADER_H

#include "Resource.h"
#include "Projector.h"
#include "Camera.h"
#include "Object.h"
#include "Material.h"

#include "Gl.h"

#include <string>
#include <vector>

using namespace std;

namespace bx {

    class Shader : public Resource {
    public:

        enum Type {
            Vertex = GL_VERTEX_SHADER,
            Fragment = GL_FRAGMENT_SHADER,
            Geometry = GL_GEOMETRY_SHADER
        };

        Shader();
        virtual ~Shader();

        Shader * clone() {
            return new Shader(*this);
        }

        static Shader * load(string vsPath, string fsPath, string gsPath);

        bool compile(string path, Type type);
        bool link();
        void start();
        static void stop();

        void send(Projector * projector, Camera * camera, Object * object);

        string getCompileLog();
        string getLinkLog();
        GLuint getProgramId();

        GLuint getUniformLocation(string varname);
        GLuint getAttribLocation(string varname);
        GLuint getUniformBlockLocation(string varname);

        bool isError();
    private:
        bool error;
        GLuint programId;
        vector<GLuint> ids;

        string compileLog;
        string linkLog;
        string readSource(string path);
    };
}

#endif	/* SHADER_H */

