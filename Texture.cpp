/* 
 * File:   Texture.cpp
 * Author: ciapunek
 * 
 * Created on July 5, 2012, 8:29 PM
 */

#include "Texture.h"
#include "Common.h"

namespace bx {

    Texture::Texture() {
        type = "Texture";
        id = 0;
        uploaded = false;
    }
    
    Texture::~Texture() {
        if (id) glDeleteTextures(1, &id);
    }

    Texture * Texture::loadTga(string path) {
        int hash = Resource::generateHash("texture_tga_" + path);
        Storage * storage = App::instance()->getStorage();
        Texture * texture = (Texture *) (storage->getResource(hash));
        
        if (texture != NULL) {
            return texture;
        }
        
        TGAImg img;

        glActiveTexture(GL_TEXTURE0);
        if (img.Load((char *) path.c_str()) == IMG_OK) {
            texture = new Texture();

            texture->path = path;
            texture->width = img.GetWidth();
            texture->height = img.GetHeight();
            texture->bpp = img.GetBPP();
            texture->size = texture->width * texture->height * (texture->bpp / 8);
            texture->data = new unsigned char[texture->size];
            memcpy(texture->data, img.GetImg(), texture->size);

            storage->addAsReference(texture, hash);
            return texture;
        }

        return NULL;
    }

    void Texture::upload() {
        if (uploaded) {
            return;
        }
        
        GLuint id = 0;
        glGenTextures(1, &id);
        glBindTexture(GL_TEXTURE_2D, id);
        this->id = id;

        if (bpp == 24) {
            glTexImage2D(GL_TEXTURE_2D, 0, 3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        } else if (bpp == 32) {
            glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        } else {
            return; // not supported
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        delete[] data;
        uploaded = true;
    }
    
    bool Texture::isUploaded() {
        return uploaded;
    }

    void Texture::start() {
        glEnable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, id);
    }

    void Texture::stop() {
        glActiveTexture(0);
        glDisable(GL_TEXTURE_2D);
    }
    
    string Texture::getPath() {
        return path;
    }
}


