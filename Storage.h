/* 
 * File:   Storage.h
 * Author: ciapunek
 *
 * Created on July 7, 2012, 1:47 PM
 */

#ifndef STORAGE_H
#define	STORAGE_H

#include "Resource.h"

#include <string>

using namespace std;

namespace bx {

    class Storage {
    public:
        struct Item {
            Resource * resource;
            bool reference;
        };
        
        Storage();
        Storage(const Storage& orig);
        virtual ~Storage();
        
        bool addAsReference(Resource * resource, int hash);
        Resource * addAsInstance(Resource * resource, int hash);
        bool hasResource(int hash);
        Resource * getResource(int hash);
        
        uint getItemCount();
        uint getInstanceCount();
        uint getHits();
        uint getMisses();
        
    private:
        vector<Item *> items;
        vector<Resource *> instances;
        
        uint hits;
        uint misses;
    };
}


#endif	/* STORAGE_H */

