/* 
 * File:   Action.h
 * Author: ciapunek
 *
 * Created on August 7, 2012, 10:57 PM
 */

#ifndef ACTION_H
#define	ACTION_H

#include "Gl.h"
#include "Actor.h"

#include <string>
#include <math.h>
#include <algorithm>

using namespace std;

namespace bx {

    class Actor;

    class Action {
    public:

        enum Priority {
            Normal,
            High,
            Immediate
        };

        Action();
        virtual ~Action();

        void setActor(Actor * actor);
        Actor * getActor(Actor * actor);

        virtual void step(float time) = 0;

        void setComplete();
        bool isComplete();
        bool isRepeatable();

        string getName();
        Priority getPriority();

    protected:
        string name;
        Priority priority;

        Actor * actor;
        bool complete;
        bool repeatable;
    };
}


#endif	/* ACTION_H */

