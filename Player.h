/* 
 * File:   Player.h
 * Author: ciapunek
 *
 * Player data, stats, settings, keybindings etc
 * 
 * Created on June 8, 2012, 4:25 PM
 */

#ifndef PLAYER_H
#define	PLAYER_H

#include "Object.h"
#include "Actor.h"

namespace bx {

    class Player : public Actor {
    public:
        Player();
        Player(const Player& orig);
        virtual ~Player();

        bool isMoving();
        bool isJumping();
    private:
        bool moving;
        bool jumping;
    };
}


#endif	/* PLAYER_H */

