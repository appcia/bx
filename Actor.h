/* 
 * File:   Actor.h
 * Author: ciapunek
 *
 * Created on August 7, 2012, 10:54 PM
 */

#ifndef ACTOR_H
#define	ACTOR_H

#include "Action.h"

#include "Object.h"
#include <queue>

using namespace std;

namespace bx {
    
    class Action;
    
    class ActionCompare {
    public:
        bool operator()(Action * a1, Action * a2);
    };
    
    class Actor : public Object {
    public:
        Actor();
        Actor(const Actor& orig);
        virtual ~Actor();

        virtual void step(float time);
        Action * getCurrentAction();
        uint getActionsLeft();
        bool addAction(Action * action);
        
        void update();
        
        void resolve();
        bool isIdle();
        
        void setJumpControlPower(float jumpControlPower);
        float getJumpControlPower() const;
        void setMaxFallSpeed(float maxFallSpeed);
        float getMaxFallSpeed() const;
        void setGravityAcceleration(float gravityAcceleration);
        float getGravityAcceleration() const;
        void setJumpLaunchVelocity(float jumpLaunchVelocity);
        float getJumpLaunchVelocity() const;
        void setMaxJumpTime(float maxJumpTime);
        float getMaxJumpTime() const;
        void setAirDragFactor(float airDragFactor);
        float getAirDragFactor() const;
        void setGroundDragFactor(float groundDragFactor);
        float getGroundDragFactor() const;
        void setMaxMovespeed(float maxMovespeed);
        float getMaxMovespeed() const;
        void setMoveAcceleration(float moveAcceleration);
        float getMoveAcceleration() const;
        void setVelocity(glm::vec2 velocity);
        void setVelocityX(float velocity);
        void setVelocityY(float velocity);
        glm::vec2 getVelocity() const;
        void setDirection(int direction);
        int getDirection() const;
        
        void removeAllActions();
    protected:
        bool idle;
        
        int direction;
        glm::vec2 velocity;
        
        // horizontal movement parameters
        float moveAcceleration;
        float maxMovespeed;
        float groundDragFactor;
        float airDragFactor;
        
        // vertical movement parameters
        float maxJumpTime;
        float jumpLaunchVelocity;
        float gravityAcceleration;
        float maxFallSpeed;
        float jumpControlPower;     
        
        Action * action;
        priority_queue<int, vector<Action *>, ActionCompare> actionQueue;
        
        Action * getNextAction();
    };
 
}


#endif	/* ACTOR_H */

