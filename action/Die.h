/* 
 * File:   Die.h
 * Author: ciapunek
 *
 * Created on August 9, 2012, 9:58 PM
 */

#ifndef DIE_H
#define	DIE_H

#include "../Actor.h"

namespace bx {

    namespace action {

        class Die : public Action {
        public:
            Die();

            void step(float time);
        private:
            float scale;
        };
    }

}



#endif	/* DIE_H */

