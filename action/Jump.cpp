/* 
 * File:   Jump.cpp
 * Author: ciapunek
 * 
 * Created on August 11, 2012, 10:58 PM
 */

#include "Jump.h"

namespace bx {
    namespace action {

        Jump::Jump() {
            name = "Jump";
            repeatable = false;
        }

        void Jump::step(float time) {
            setComplete();
        }
    }
}
