/* 
 * File:   Demo.cpp
 * Author: ciapunek
 * 
 * Created on August 11, 2012, 5:17 PM
 */

#include "Demo.h"

namespace bx {

    namespace action {

        Demo::Demo() {
            name = "Demo";
            priority = Normal;
            
            elapsedTime = 0;
        }
        
        void Demo::step(float time) {         
            elapsedTime += time;
            
            float y = actor->getY();
            y += sin(elapsedTime / 10) / 100;
            actor->setY(y);
            
            actor->setScale(
                abs(sin(elapsedTime))),
                abs(sin(elapsedTime)),
                abs(sin(elapsedTime));
        }
    }

}


