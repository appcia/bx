/* 
 * File:   Float.cpp
 * Author: hayalet
 * 
 * Created on 12 sierpień 2012, 19:07
 */

#include "Float.h"

namespace bx {
    namespace action {

        Float::Float() {
            name = "Float";
            repeatable = "true";
            
            elapsedTime = 0;
        }
        
        void Float::step(float time) {
            elapsedTime += time;
            float rotY = actor->getRotationY();
            float y = actor->getY();
            
            y += (sin(elapsedTime) / 400);
            int speedCoeff = 1 + rand() % 7;
            rotY += speedCoeff * sin(elapsedTime + 100);
            
            actor->setRotationY(rotY);
            actor->setY(y);
        }
    }
}

