/* 
 * File:   Sleep.cpp
 * Author: ciapunek
 * 
 * Created on August 11, 2012, 9:41 PM
 */

#include "Sleep.h"

namespace bx {
    namespace action {

        Sleep::Sleep(float duration) {
            name = "Sleep";
            priority = Normal;
            
            this->duration = duration;
        }
        
        void Sleep::step(float time) {
            duration -= time;
            
            if (duration <= 0) {
                setComplete();
            }
        }


    }
}

