/* 
 * File:   Demo.h
 * Author: ciapunek
 *
 * Created on August 11, 2012, 5:17 PM
 */

#ifndef DEMO_H
#define	DEMO_H

#include "../Action.h"

namespace bx {
    namespace action {

        class Demo : public Action {
        public:
            Demo();
            
            void step(float time);
        private:
            float elapsedTime;
        };
    }
}



#endif	/* DEMO_H */

