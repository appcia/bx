/* 
 * File:   All.h
 * Author: ciapunek
 *
 * Created on August 11, 2012, 8:19 PM
 */

#ifndef ALL_H
#define	ALL_H

#include "Demo.h"
#include "Die.h"
#include "Turnaround.h"
#include "Move.h"
#include "Sleep.h"
#include "Stop.h"
#include "Float.h"


#endif	/* ALL_H */

