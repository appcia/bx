/* 
 * File:   Die.cpp
 * Author: ciapunek
 * 
 * Created on August 9, 2012, 9:58 PM
 */

#include "Die.h"

namespace bx {

    namespace action {

        Die::Die() {
            name = "Die";
            priority = Immediate;
        }

        void Die::step(float time) {
            scale = actor->getScaleX();
            if (scale > 0) {
                scale -= 0.01;
                actor->setScale(scale);
            } else {
                actor->hide();
                this->setComplete();
            }
        }
    }



}

