/* 
 * File:   Move.h
 * Author: hayalet
 *
 * Created on 11 sierpień 2012, 21:00
 */

#ifndef MOVE_H
#define	MOVE_H

#include "../Action.h"

namespace bx {
    namespace action {

        class Move : public Action {
        public:
            enum Direction {
                Left = -1,
                Idle = 0,
                Right = 1
            };
            
            Move(Direction direction);
            
            void step(float time);
        private:
            Direction direction;
        };
    }
}

#endif	/* MOVE_H */

