/* 
 * File:   Jump.h
 * Author: ciapunek
 *
 * Created on August 11, 2012, 10:58 PM
 */

#ifndef JUMP_H
#define	JUMP_H

#include "../Action.h"

namespace bx {
    namespace action {

        class Jump : public Action {
        public:
            Jump();

            void step(float time);
        private:
        };
    }
}



#endif	/* JUMP_H */

