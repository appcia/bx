/* 
 * File:   Stop.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 13:26
 */

#ifndef STOP_H
#define	STOP_H

#include "../Action.h"

namespace bx {
    namespace action {

        class Stop : public Action {
        public:
            Stop();
            
            void step(float time);
        private:
            float elapsedTime;
        };
    }
}



#endif	/* STOP_H */

