/* 
 * File:   Sleep.h
 * Author: ciapunek
 *
 * Created on August 11, 2012, 9:41 PM
 */

#ifndef SLEEP_H
#define	SLEEP_H

#include "../Action.h"

namespace bx {
    namespace action {

        class Sleep : public Action {
        public:
            Sleep(float duration);
            
            void step(float time);
        private:
            float duration;
        };
    }
}


#endif	/* SLEEP_H */

