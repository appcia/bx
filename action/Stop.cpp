/* 
 * File:   Stop.cpp
 * Author: hayalet
 * 
 * Created on 12 sierpień 2012, 13:26
 */

#include "../Util.h"
#include "Stop.h"


namespace bx {
    namespace action {
        Stop::Stop() {
            elapsedTime = 0;
        }
        
        void Stop::step(float time) {
            elapsedTime += time;
            actor->setDirection(0);
            actor->setVelocityX(0);
            this->setComplete();
        }
    }
}

