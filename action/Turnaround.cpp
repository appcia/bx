/* 
 * File:   Turnaround.cpp
 * Author: ciapunek
 * 
 * Created on August 9, 2012, 8:07 PM
 */

#include "../Util.h"
#include "Turnaround.h"

namespace bx {

    namespace action {

        Turnaround::Turnaround(Direction direction) {
            name = "Turnaround";
            priority = Normal;

            this->direction = direction;
            elapsedTime = 0;
        }

        void Turnaround::step(float time) {
            if (elapsedTime < 0.3) {
                actor->setDirection(direction);
                elapsedTime += time;
                float rotY = actor->getRotationY();

                if (direction == Right) {
                    rotY += 3.0;
                } else if (direction == Left) {
                    rotY -= 3.0;
                }
                actor->setRotationY(rotY);
            } else {
               this->complete = true; 
            }
        }
    }


}
