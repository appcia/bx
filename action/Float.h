/* 
 * File:   Float.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 19:07
 */

#ifndef FLOAT_H
#define	FLOAT_H

#include "../Action.h"

namespace bx {
    namespace action {

        class Float : public Action{
        public:
            Float();
            
            void step(float time);
        private:
            float elapsedTime;
        };
    }
}



#endif	/* FLOAT_H */

