/* 
 * File:   Move.cpp
 * Author: hayalet
 *
 * Created on 11 sierpień 2012, 21:00
 */

#include "../Util.h"
#include "Move.h"

namespace bx {

    namespace action {

        Move::Move(Direction direction) {
            name = "Move";
            priority = Normal;
            repeatable = false;

            this->direction = direction;
        }

        void Move::step(float time) {
            actor->setDirection(direction);
            
            float xVelocity = actor->getVelocity().x;
            float maxMoveSpeed = actor->getMaxMovespeed();
            
            if (xVelocity < maxMoveSpeed && xVelocity > -maxMoveSpeed) {
                xVelocity += actor->getDirection() * actor->getMoveAcceleration() * time;
            } else if (xVelocity > maxMoveSpeed) {
                xVelocity = maxMoveSpeed;
            } else if (xVelocity < -maxMoveSpeed) {
                xVelocity = -maxMoveSpeed;
            }
            
            actor->setVelocityX(xVelocity);

            float x = actor->getX();
            x += xVelocity * time;
            actor->setX(x);
        }
    }

}



