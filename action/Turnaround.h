/* 
 * File:   Turnaround.h
 * Author: ciapunek
 *
 * Created on August 9, 2012, 8:07 PM
 */

#ifndef TURNAROUND_H
#define	TURNAROUND_H

#include "../Actor.h"

namespace bx {

    namespace action {

        class Turnaround : public Action {
        public:
            enum Direction {
                Left = -1,
                Idle = 0,
                Right = 1
            };
            
            Turnaround(Direction direction);

            glm::mat4 getPosition();
            void step(float time);
        private:
            float direction;
            float elapsedTime;
        };
    }


}


#endif	/* TURNAROUND_H */

