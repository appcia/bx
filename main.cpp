/* 
 * File:   main.cpp
 * Author: ciapunek
 *
 * Main file
 * 
 * Created on June 4, 2012, 12:48 PM
 */

#include "App.h"

int main() {
    bx::App * app = bx::App::instance();
    return app->exec();
}

