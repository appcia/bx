/* 
 * File:   Projector.cpp
 * Author: ciapunek
 * 
 * Created on June 18, 2012, 5:40 PM
 */

#include "Projector.h"

namespace bx {

    Projector::Projector() {
        setDefaults();
    }

    Projector::Projector(const Projector& orig) {
    }

    Projector::~Projector() {
    }

    /**
     * Startup projection settings
     */
    void Projector::setDefaults() {
        aspect = 4.0f / 3.0f;
        fowY = 45.0f;
        zNear = 0.1f;
        zFar = 100.0;
    }

    glm::mat4 Projector::getMatrix() {
        return glm::perspective(fowY, aspect, zNear, zFar);
    }
}
