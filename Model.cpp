/* 
 * File:   Model.cpp
 * Author: ciapunek
 * 
 * Created on June 12, 2012, 8:06 PM
 */

#include "Model.h"

#include <cstdio>
#include <cstring>

#define TMP_BUFFER 1023

namespace bx {

    Model::Model() {
        file = "";
        uploaded = false;
        
        locVertex = -1;
        locNormal = -1;
        locUv = -1;
    }

    Model::Model(const Model& orig) {
    }

    Model::~Model() {
    }

    string Model::getFile() {
        return file;
    }

    Model * Model::loadObj(string path) {

        FILE * file = fopen(path.c_str(), "r");
        if (file == NULL) {
            return NULL;
        }

        vector<unsigned int> vertexIndices;
        vector<unsigned int> uvIndices;
        vector<unsigned int> normalIndices;
        vector<glm::vec3> tmpVertices;
        vector<glm::vec2> tmpUvs;
        vector<glm::vec3> tmpNormals;

        Model * model = new Model();
        model->file = path;

        while (true) {

            int res = -1;
            char line[128];

            res = fscanf(file, "%s", line);
            if (res == EOF) {
                break;
            }

            if (strcmp(line, "v") == 0) {
                glm::vec3 vertex;
                res = fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
                tmpVertices.push_back(vertex);
            } else if (strcmp(line, "vt") == 0) {
                glm::vec2 uv;
                res = fscanf(file, "%f %f\n", &uv.x, &uv.y);
                uv.y = 1 - uv.y;
                tmpUvs.push_back(uv);
            } else if (strcmp(line, "vn") == 0) {
                glm::vec3 normal;
                res = fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
                tmpNormals.push_back(normal);
            } else if (strcmp(line, "f") == 0) {
                string vertex1, vertex2, vertex3;
                unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
                int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
                if (matches != 9) {
                    delete model;
                    return NULL;
                }

                // Process data
                vertexIndices.push_back(vertexIndex[0]);
                vertexIndices.push_back(vertexIndex[1]);
                vertexIndices.push_back(vertexIndex[2]);
                uvIndices .push_back(uvIndex[0]);
                uvIndices .push_back(uvIndex[1]);
                uvIndices .push_back(uvIndex[2]);
                normalIndices.push_back(normalIndex[0]);
                normalIndices.push_back(normalIndex[1]);
                normalIndices.push_back(normalIndex[2]);
            } else {
                // Probably a comment, eat up the rest of the line
                char stupidBuffer[TMP_BUFFER];
                char * tmp = fgets(stupidBuffer, TMP_BUFFER, file);
            }

        }

        // For each vertex of each triangle
        for (unsigned int i = 0; i < vertexIndices.size(); i++) {

            // Get the indices of its attributes
            unsigned int vertexIndex = vertexIndices[i];
            unsigned int uvIndex = uvIndices[i];
            unsigned int normalIndex = normalIndices[i];

            // Get the attributes thanks to the index
            glm::vec3 vertex = tmpVertices[ vertexIndex - 1 ];
            glm::vec2 uv = tmpUvs[ uvIndex - 1 ];
            glm::vec3 normal = tmpNormals[ normalIndex - 1 ];

            // Put the attributes in buffers
            model->vertices.push_back(vertex);
            model->uvs.push_back(uv);
            model->normals.push_back(normal);
        }
        return model;
    }
    
    const vector<glm::vec3> & Model::getVertices() {
        return this->vertices;
    }
    
    void Model::upload(Shader * shader) {
        if (uploaded) return;
        
        if (vertices.size() > 0) {
            glGenBuffers(1, &vboVertices);
            glBindBuffer(GL_ARRAY_BUFFER, vboVertices);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof (vertices[0]), vertices.data(), GL_STATIC_DRAW);
        }

        if (uvs.size() > 0) {
            glGenBuffers(1, &vboUvs);
            glBindBuffer(GL_ARRAY_BUFFER, vboUvs);
            glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof (uvs[0]), uvs.data(), GL_STATIC_DRAW);
        }

        if (normals.size() > 0) {
            glGenBuffers(1, &vboNormals);
            glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
            glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof (normals[0]), normals.data(), GL_STATIC_DRAW);
        }

        locVertex = shader->getAttribLocation("Vertex");
        locUv = shader->getAttribLocation("uvCoords");
        locNormal = shader->getAttribLocation("Normals");
        
        uploaded = true;
    }
    
    bool Model::isUploaded() {
        return uploaded;
    }
    
    /**
     * Send uniforms to object shader
     * 
     * @param object
     * @param projector
     * @param camera
     */
    void Model::send(Object * object, Projector * projector, Camera * camera) {
        Material * material = object->getMaterial();
        Shader * shader = object->getShader();
        
        glUniformMatrix4fv(shader->getUniformLocation("P"), 1, GL_FALSE, glm::value_ptr(projector->getMatrix()));
        glUniformMatrix4fv(shader->getUniformLocation("V"), 1, GL_FALSE, glm::value_ptr(camera->getMatrix()));
        glUniformMatrix4fv(shader->getUniformLocation("M"), 1, GL_FALSE, glm::value_ptr(object->getMatrix()));
        
        glm::vec4 color = material->getAmbientColor();
        glUniform4f(shader->getUniformLocation("material.ambientColor"),color.x,color.y,color.z,color.w);
        color = material->getDiffuseColor();
        glUniform4f(shader->getUniformLocation("material.diffuseColor"),color.x,color.y,color.z,color.w);
        color = material->getSpecularColor();
        glUniform4f(shader->getUniformLocation("material.specularColor"),color.x,color.y,color.z,color.w);
        glUniform1f(shader->getUniformLocation("material.illumination"),material->getIllumination());
        glUniform1f(shader->getUniformLocation("material.shininess"),material->getShininess());
    }
    
    void Model::draw() {
        // Bind buffers
        if (vboVertices != 0) {
            glBindBuffer(GL_ARRAY_BUFFER, vboVertices);
            glEnableVertexAttribArray(locVertex);
            glVertexAttribPointer(locVertex, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        }
        if (vboNormals != 0) {
            glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
            glEnableVertexAttribArray(locNormal);
            glVertexAttribPointer(locNormal, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        }
        if (vboUvs) {
            glBindBuffer(GL_ARRAY_BUFFER, vboUvs);
            glEnableVertexAttribArray(locUv);
            glVertexAttribPointer(locUv, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        }      
        
        // Draw
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());
        
        // Clean
        if (vboVertices != 0) glDisableVertexAttribArray(locVertex);
        if (vboNormals != 0) glDisableVertexAttribArray(locNormal);
        if (vboUvs != 0) glDisableVertexAttribArray(locUv);
    }
}
