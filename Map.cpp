/* 
 * File:   Map.cpp
 * Author: ciapunek
 * 
 * 
 * Created on June 8, 2012, 4:26 PM
 */

#include "Map.h"

namespace bx {

    Map::Map() {
        name = "Map";
    }

    Map::~Map() {
    }
    
    string Map::getName() {
        return name;
    }
    
    /**
     * Load map from file
     */
    Map * Map::load(string filename) {
        // @todo complete this function
    }
    
    /**
     * On default, 'use' should create objects basing on configuration 
     * loaded from file, but could be derrived in case of creating
     * objects on hand
     */
    void Map::use(Scene * scene) {
        // @todo complete this function
    }
}
