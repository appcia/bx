/* 
 * File:   Point.h
 * Author: ciapunek
 *
 * Created on August 12, 2012, 12:55 AM
 */

#ifndef POINT_H
#define	POINT_H

#include "Gl.h"

namespace bx {

    class Point {
    public:
        Point();
        virtual ~Point();

        glm::vec3 getPosition();
        void setPosition(float x, float y, float z);

        void setX(float x);
        float getX() const;
        void setZ(float z);
        float getZ() const;
        void setY(float y);
        float getY() const;
    protected:
        float x, y, z;
    };
}



#endif	/* POINT_H */

