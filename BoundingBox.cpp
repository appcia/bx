/* 
 * File:   BoundingBox.cpp
 * Author: hayalet
 * 
 * Created on August 7, 2012, 10:48 PM
 */

#include "BoundingBox.h"
#include "Util.h"

namespace bx {

    BoundingBox::BoundingBox(const vector <glm::vec3> vertices, const glm::mat4 matrix) {
        GLfloat minX, minY, minZ, maxX, maxY, maxZ;
        minX = maxX = vertices[0].x;
        minY = maxY = vertices[0].y;
        minZ = maxZ = vertices[0].z;
        for (int i = 0; i < vertices.size(); i++) {
            if (vertices[i].x < minX) minX = vertices[i].x;
            if (vertices[i].x > maxX) maxX = vertices[i].x;
            if (vertices[i].y < minY) minY = vertices[i].y;
            if (vertices[i].y > maxY) maxY = vertices[i].y;
            if (vertices[i].z < minZ) minZ = vertices[i].z;
            if (vertices[i].z > maxZ) maxZ = vertices[i].z;
        }

        initmin = glm::vec3(minX, minY, minZ);
        initmax = glm::vec3(maxX, maxY, maxZ);

        transform(matrix);

        calculateSize();
        calculateCenter();
    }

    BoundingBox::~BoundingBox() {
    }

    void BoundingBox::calculateSize() {
        size.x = max.x - min.x;
        size.y = max.y - min.y;
        size.z = max.z - min.z;
    }

    void BoundingBox::calculateCenter() {
        center.x = (min.x + max.x) / 2;
        center.y = (min.y + max.y) / 2;
        center.z = (min.z + max.z) / 2;
    }

    void BoundingBox::transform(const glm::mat4 matrix) {
        glm::vec4 tmpMin = matrix * glm::vec4(initmin,1.0);
        min = glm::vec3(tmpMin.x,tmpMin.y,tmpMin.z);
        glm::vec4 tmpMax = matrix * glm::vec4(initmax,1.0);
        max = glm::vec3(tmpMax.x,tmpMax.y,tmpMax.z);
    }
    
    BoundingBox::Rectangle BoundingBox::get2DRectangle() {
        struct BoundingBox::Rectangle rect(min.x, min.y, max.x - min.x, max.y - min.y);
        Util::print("%f %f %f %f\n",rect.x,rect.y,rect.width,rect.height);
        return rect;
    }
    
    bool BoundingBox::check2DCollision(BoundingBox * that) {
        BoundingBox::Rectangle rect1 = this->get2DRectangle();
        BoundingBox::Rectangle rect2 = that->get2DRectangle();
        if(rect1.x >= rect2.x + rect2.width || rect1.x + rect1.width <= rect2.x || rect1.y >= rect2.y + rect2.height || rect1.y + rect1.height <= rect2.y) {
            return false;
        }
        return true;
    }
}
