/* 
 * File:   Config.cpp
 * Author: ciapunek
 * 
 * Configuration data container
 * 
 * Created on June 4, 2012, 2:45 PM
 */

#include "Config.h"
#include "Util.h"

namespace bx {

    Config::Config() {
    }

    Config::Config(const Config& orig) {
    }

    Config::~Config() {
    }

    Config * Config::loadDefault() {
        Config * config = new Config();
        
        config->setInt("debugMode", 0);

        config->set("windowTitle", "World of boxes");
        config->setInt("windowStyle", 4); // relative to sf::Style (4-windowed, 8-fullscreen)
        
        config->setInt("sceneWidth", 1024);
        config->setInt("sceneHeight", 768); 
        
        config->setInt("framerateLimit", 90); // 0 - no limit, n [fps]
        config->setInt("verticalSync", 0);
        
        config->setInt("antialiasingLevel", 0);
        config->setInt("depthBits", 24);
        config->setInt("stencilBits", 8);
        
        config->setFloat("mouseMoveSpeed", 15.0f);
        config->setFloat("mouseZoomSpeed", 5.0f);
        
        config->setFloat("cameraMoveSpeed", 7.0f);

        return config;
    }

    Config * Config::loadFromFile(string filename) {

    }

    string Config::operator[](string name) {
        return get(name);
    }

    void Config::set(string name, string value) {
        data[name] = value;
    }

    string Config::get(string name) {
        return data[name];
    }

    void Config::setInt(string name, int value) {
        data[name] = Util::intToStr(value);
    }

    int Config::getInt(string name) {
        return Util::strToInt(data[name]);
    }

    void Config::setFloat(string name, float value) {
        data[name] = Util::floatToStr(value);
    }

    float Config::getFloat(string name) {
        return Util::strToFloat(data[name]);
    }
}

