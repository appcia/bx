/* 
 * File:   Util.cpp
 * Author: ciapunek
 * 
 * Created on July 5, 2012, 7:51 PM
 */

#include "Util.h"

#include "App.h"
#include <cstdarg>

namespace bx {

    void Util::logTime() {
        App * app = App::instance();

        static int id = 0;
        static float last = 0;
        
        const float time = app->getClock()->GetElapsedTime();

        id++;
        cout << "(" << id << ") " << time << "s " << "(+" << (time - last) << " s)\n";
        last = time;
    }
    
    void Util::debug(string text) {
        logTime();
        cout << text << "\n";
    }
    
    void Util::print(string format, ...) {
        const char * tmp = format.c_str();
        
        va_list argPtr;
        va_start(argPtr, tmp);

        vprintf(tmp, argPtr);

        va_end(argPtr);
    }

    string Util::intToStr(int value) {
        char tmp[255];
        sprintf((char *) tmp, "%d", value);
        return tmp;
    }

    int Util::strToInt(string value) {
        int num;
        sscanf(value.c_str(), "%d", &num);
        return num;
    }

    string Util::floatToStr(float value) {
        char tmp[255];
        sprintf((char *) tmp, "%f", value);
        return tmp;
    }

    int Util::strToFloat(string value) {
        float num;
        sscanf(value.c_str(), "%f", &num);
        return num;
    }

    int Util::hashInt(string value) {
        int hash = 0;
        for (int i = 0; i < value.length(); i++) {
            hash += (int) value[i];
        }

        return hash;
    }
}
