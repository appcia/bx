/* 
 * File:   Resource.cpp
 * Author: ciapunek
 * 
 * Created on July 7, 2012, 1:38 PM
 */

#include "Resource.h"
#include "Common.h"

namespace bx {

    Resource::Resource() {
        type = "Resource";
        hash = 0;
    }

    Resource::Resource(const Resource& orig) {
    }

    Resource::~Resource() {
    }
    
    int Resource::generateHash(string value) {
        return Util::hashInt(value);
    }
    
    void Resource::setHash(int hash) {
        this->hash = hash;
    }
    
    int Resource::getHash() {
        return hash;
    }


}
