/* 
 * File:  Material.cpp
 * Author: hayalet, ciapunek
 *
 * Created on 27 czerwiec 2012, 13:07
 */

#include "Material.h"
#include "Texture.h"
#include "Common.h"
#include "App.h"

#include <cstdio>
#include <cstring>

namespace bx {

    Material::Material() {
        type = "Material";
        texture = 0;
        normalTexture = 0;
        path = "";
    }

    Material::~Material() {
        if (texture) delete texture;
        if (normalTexture) delete normalTexture;
    }

    Material * Material::loadMtl(string path) {
        int hash = Resource::generateHash("material_mtl_" + path);
        Storage * storage = App::instance()->getStorage();
        Material * material = (Material *) storage->getResource(hash);
        
        // Storage hit
        if (material != NULL) {
            return material;
        }

        // Load material and add to storage
        FILE * file = fopen(path.c_str(), "r");
        if (file == NULL) {
            return NULL;
        }

        material = new Material();
        material->path = path;

        while (true) {
            int res = -1;
            char line[128];
            res = fscanf(file, "%s", line);
            if (res == EOF) {
                break;
            }
            if (strcmp(line, "Ns") == 0) {
                res = fscanf(file, "%f\n", &material->shininess);
            } else if (strcmp(line, "Ka") == 0) {
                glm::vec4 ambientColor;
                res = fscanf(file, "%f %f %f\n", &ambientColor.r, &ambientColor.g, &ambientColor.b);
                ambientColor.a = 1.0;
                material->ambientColor = ambientColor;
            } else if (strcmp(line, "Kd") == 0) {
                glm::vec4 diffuseColor;
                res = fscanf(file, "%f %f %f\n", &diffuseColor.r, &diffuseColor.g, &diffuseColor.b);
                diffuseColor.a = 1.0;
                material->diffuseColor = diffuseColor;
            } else if (strcmp(line, "Ks") == 0) {
                glm::vec4 specularColor;
                res = fscanf(file, "%f %f %f\n", &specularColor.r, &specularColor.g, &specularColor.b);
                specularColor.a = 1.0;
                material->specularColor = specularColor;
            } else if (strcmp(line, "illum") == 0) {
                int illum;
                res = fscanf(file, "%d\n", &illum);
                if (illum == 2) {
                    material->illumination = true;
                } else {
                    material->illumination = false;
                }
            }
        }

        material = (Material *) storage->addAsInstance(material, hash);
        // storage->addAsReference(material, hash);
        
        return material;
    }

    void Material::loadTexture(string path) {
        texture = Texture::loadTga(path);
    }

    void Material::loadNormalTexture(string path) {
        normalTexture = Texture::loadTga(path);
    }

    bool Material::isNormalTexture() {
        return normalTexture != 0;
    }

    bool Material::isBumpMapping() {
        return bumpMapping;
    }

    Texture * Material::getTexture() {
        return texture;
    }

    Texture * Material::getNormalTexture() {
        return normalTexture;
    }

    float Material::getShininess() {
        return shininess;
    }

    float Material::getIllumination() {
        return illumination;
    }

    glm::vec4 Material::getAmbientColor() {
        return ambientColor;
    }

    glm::vec4 Material::getDiffuseColor() {
        return diffuseColor;
    }
    

    glm::vec4 Material::getSpecularColor() {
        return specularColor;
    }

    void Material::setDiffuseColor(glm::vec4 diffuseColor) {
        this->diffuseColor = diffuseColor;
    }

}
