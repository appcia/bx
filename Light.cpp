/* 
 * File:   Light.cpp
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 17:07
 */

#include "Light.h"

namespace bx {

    Light::Light() {
    }

    Light::~Light() {
    }

    glm::vec4 Light::getDiffuseColor() {
        return diffuseColor;
    }

    void Light::setDiffuseColor(float r, float g, float b, float alpha) {
        this->diffuseColor = glm::vec4(r, g, b, alpha);
    }

    glm::vec4 Light::getSpecularColor() {
        return specularColor;
    }

    void Light::setSpecularColor(float r, float g, float b, float alpha) {
        this->specularColor = glm::vec4(r, g, b, alpha);
    }

    void Light::setColor(float r, float g, float b, float alpha) {
        this->diffuseColor = glm::vec4(r, g, b, alpha);
        this->specularColor = glm::vec4(r, g, b, alpha);
    }

    Light::Type Light::getType() {
        return type;
    }
}


