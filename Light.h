/* 
 * File:   Light.h
 * Author: hayalet
 *
 * Created on 12 sierpień 2012, 17:02
 */

#ifndef LIGHT_H
#define	LIGHT_H

#include "Gl.h"
#include "Point.h"

namespace bx {

    class Light : public Point {
    public:

        enum Type {
            Directional,
            Punctual
        };

        Light();
        virtual ~Light();

        glm::vec4 getDiffuseColor();
        void setDiffuseColor(float r, float g, float b, float alpha);

        glm::vec4 getSpecularColor();
        void setSpecularColor(float r, float g, float b, float alpha);

        void setColor(float r, float g, float b, float alpha);

        Type getType();
    protected:
        Type type;
        glm::vec4 diffuseColor;
        glm::vec4 specularColor;
    };
}

#endif	/* LIGHT_H */

